package hr.zeroinfinity.focustowin.config.logger

import android.util.Log

object F2WLogger {

    private const val ERROR_TAG = "F2W/error"
    private const val INFO_TAG = "F2W/info"
    private const val WARN_TAG = "F2W/warn"
    private const val DEBUG_TAG = "F2W/debug"

    fun info(message: String?) = Log.i(INFO_TAG, message)

    fun warn(message: String?) = Log.w(WARN_TAG, message)

    fun error(message: String?) = Log.e(ERROR_TAG, message)

    fun debug(message: String?) = Log.d(DEBUG_TAG, message)
}
package hr.zeroinfinity.focustowin.config.lifecycle

import android.app.usage.UsageEvents
import android.app.usage.UsageStatsManager
import android.content.Context
import java.util.*

object ProcessHelper {

    fun areOtherAppsRunning(context: Context, currentAppPackageName: String, from: Date): Boolean {
        val usageStatsManager = context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager

        val usageEvents = usageStatsManager.queryEvents(from.time, Date().time)
        val event = UsageEvents.Event()
        while (usageEvents.getNextEvent(event)) {
            if (event.eventType == UsageEvents.Event.MOVE_TO_FOREGROUND && currentAppPackageName != event.packageName) {
                return true
            }
        }

        return false
    }
}
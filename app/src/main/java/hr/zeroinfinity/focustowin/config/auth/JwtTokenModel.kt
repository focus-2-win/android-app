package hr.zeroinfinity.focustowin.config.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import hr.zeroinfinity.focustowin.config.gson.GsonContext

data class JwtTokenModel(
    @SerializedName("access_token")
    @Expose
    var accessToken: String,

    @SerializedName("token_type")
    @Expose
    var tokenType: String,

    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String,

    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int,

    @SerializedName("scope")
    @Expose
    var scope: String,

    @SerializedName("user_data")
    @Expose
    var userData: Map<String, String>,

    @SerializedName("jti")
    @Expose
    var jti: String
) {

    override fun toString(): String {
        return GsonContext.instance.toJson(this)
    }

    companion object {
        fun fromJSON(json: String?): JwtTokenModel? {
            if (json == null) return null

            return GsonContext.instance.fromJson(json, JwtTokenModel::class.java)
        }
    }
}
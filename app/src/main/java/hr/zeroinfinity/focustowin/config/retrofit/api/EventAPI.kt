package hr.zeroinfinity.focustowin.config.retrofit.api

import hr.zeroinfinity.focustowin.BuildConfig
import hr.zeroinfinity.focustowin.config.retrofit.RetrofitContext
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.Response
import hr.zeroinfinity.focustowin.model.internal.paging.PageModel
import io.reactivex.Observable
import retrofit2.http.*

interface EventAPI {

    @GET("/event")
    fun getByChildIdPageable(
        @Query("childId") childId: Long,
        @Query("page") page: Int = 0,
        @Query("pageSize") pageSize: Int = BuildConfig.PAGE_SIZE
    ): Observable<PageModel<Event>>

    @POST("/event")
    fun create(@Body event: Event): Observable<Response<Long>>

    @PUT("/event")
    fun update(@Body event: Event): Observable<Response<Long>>

    @DELETE("/event")
    fun delete(@Query("id") id: Long): Observable<Response<Long>>

    companion object {
        val instance: EventAPI by lazy {
            RetrofitContext.instance.create(EventAPI::class.java)
        }

        const val PAGE_SIZE = 10
    }
}
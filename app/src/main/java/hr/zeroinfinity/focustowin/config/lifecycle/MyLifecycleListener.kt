package hr.zeroinfinity.focustowin.config.lifecycle

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import hr.zeroinfinity.focustowin.common.pattern.Listener
import hr.zeroinfinity.focustowin.common.pattern.Subject
import hr.zeroinfinity.focustowin.common.pattern.SubjectAdapter

object MyLifecycleListener : LifecycleObserver, Subject<Boolean> {

    private val adapter = SubjectAdapter<Boolean>()

    override fun addListener(listener: Listener<Boolean>) {
        adapter.addListener(listener)
    }

    override fun removeListener(listener: Listener<Boolean>) {
        adapter.removeListener(listener)
    }

    override fun notifyListeners(data: Boolean) {
        adapter.notifyListeners(data)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onMoveToForeground() {
        //Log.d("SampleLifecycle", "Returning to foreground…")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onMoveToBackground() {
        //Log.d("SampleLifecycle", "Moving to background…")
    }
}
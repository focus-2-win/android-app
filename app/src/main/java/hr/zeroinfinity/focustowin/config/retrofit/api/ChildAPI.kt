package hr.zeroinfinity.focustowin.config.retrofit.api

import hr.zeroinfinity.focustowin.config.retrofit.RetrofitContext
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.Response
import io.reactivex.Observable
import retrofit2.http.*

interface ChildAPI {

    @GET("/child")
    fun get(@Query("id") id: Long): Observable<Child>

    @GET("/child/new")
    fun create(): Observable<Child>

    @PUT("/child")
    fun update(@Body child: Child): Observable<Response<Long>>

    @DELETE("/child")
    fun delete(@Query("id") id: Long): Observable<Response<Long>>

    companion object {
        val instance: ChildAPI by lazy {
            RetrofitContext.instance.create(ChildAPI::class.java)
        }
    }
}
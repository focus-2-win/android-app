package hr.zeroinfinity.focustowin.config

//intent codes
const val ID_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.ID"
const val IS_CHILD_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.IS_CHILD"
const val CHILD_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.CHILD"
const val CHILD_ID_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.CHILD_ID"
const val PARENT_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.PARENT"
const val INDEX_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.INDEX"
const val EVENT_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.EVENT"
const val FAMILY_ID_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.FAMILY_ID"
const val WISHES_LIST_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.WISHES_LIST"
const val EVENT_INFO_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.EVENT_INFO"
const val EVENT_STATE_CHANGED_INTENT_CODE = "hr.zeroinfinity.focustowin.config.intent.EVENT_STATE_CHANGED"

//intent filters
const val EVENT_INFO_INTENT_FILTER = "hr.zeroinfinity.focustowin.config.intentFilter.EVENT_INFO"
const val EVENT_STATE_CHANGED_INTENT_FILTER = "hr.zeroinfinity.focustowin.config.intentFilter.EVENT_STATE_CHANGED"

//shared preferences keys
const val PARENT_TOKEN_KEY_SHARED_PREFERENCES = "hr.zeroinfinity.focustowin.config.shared.PARENT_TOKEN"
const val CHILD_TOKEN_KEY_SHARED_PREFERENCES = "hr.zeroinfinity.focustowin.config.shared.CHILD_TOKEN"
const val APP_MODE_SHARED_PREFERENCES = "hr.zeroinfinity.focustowin.config.shared.APP_MODE"
const val EVENT_SHARED_PREFERENCES = "hr.zeroinfinity.focustowin.config.shared.EVENT"

enum class AppMode {
    CHILD, PARENT, NONE
}

var API_KEY: String? = null
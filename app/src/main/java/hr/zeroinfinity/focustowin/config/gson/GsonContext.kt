package hr.zeroinfinity.focustowin.config.gson

import com.google.gson.Gson
import com.google.gson.GsonBuilder

object GsonContext {

    private const val GSON_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"

    val instance: Gson = GsonBuilder()
        .setDateFormat(GSON_DATE_FORMAT)
        .create()
}
package hr.zeroinfinity.focustowin.config.retrofit

import hr.zeroinfinity.focustowin.BuildConfig
import hr.zeroinfinity.focustowin.config.API_KEY
import hr.zeroinfinity.focustowin.config.AppMode
import hr.zeroinfinity.focustowin.config.auth.AuthCallExecutor
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.event.EventBus
import hr.zeroinfinity.focustowin.config.event.UnauthorizedEvent
import hr.zeroinfinity.focustowin.config.gson.GsonContext
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

object RetrofitContext {
    private const val AUTHORIZATION = "Authorization"
    private const val ACCEPT_LANGUAGE = "Accept-Language"
    private const val API_KEY_HEADER = "F2W-Api-Key"

    val instance: Retrofit by lazy(
        Retrofit.Builder()
            .baseUrl(BuildConfig.BACKEND_URL)
            .client(
                OkHttpClient.Builder()
                    .authenticator(Authenticator { _, response ->
                        if (AuthContextHolder.instance.mode == AppMode.PARENT && AuthCallExecutor.refreshToken())
                            return@Authenticator response
                                .request()
                                .newBuilder()
                                .addHeader(AUTHORIZATION, AuthContextHolder.instance.getAuthorizationHeader())
                                .addHeader(ACCEPT_LANGUAGE, Locale.getDefault().language)
                                .addHeader(API_KEY_HEADER, API_KEY!!)
                                .build()
                        else {
                            EventBus.post(UnauthorizedEvent)
                            return@Authenticator null
                        }
                    })
                    .addInterceptor(Interceptor {
                        val request = it
                            .request()
                            .newBuilder()
                            .addHeader(
                                AUTHORIZATION,
                                if (it.request().url().url().path.endsWith("/oauth/token"))
                                    AuthContextHolder.instance.getBasicAuthorizationHeader()
                                else
                                    AuthContextHolder.instance.getAuthorizationHeader()
                            )
                            .addHeader(ACCEPT_LANGUAGE, Locale.getDefault().language)
                            .build()

                        return@Interceptor it.proceed(request)
                    }).build()
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonContext.instance
                )
            )::build
    )
}


package hr.zeroinfinity.focustowin.config.retrofit.api

import hr.zeroinfinity.focustowin.config.retrofit.RetrofitContext
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.Family
import hr.zeroinfinity.focustowin.model.Request
import hr.zeroinfinity.focustowin.model.User
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface FamilyAPI {

    @POST("/family")
    fun createFamily(): Observable<Family>

    @GET("/family/parent")
    fun getFamily(): Observable<Family>

    @POST("/family/parent")
    fun addParent(@Body request: Request<Long>): Observable<User>

    @GET("/family/child")
    fun getFamily(@Query("childId") childId: Long): Observable<Family>

    @POST("/family/child")
    fun addChild(@Body request: Child): Observable<Child>

    companion object {
        val instance: FamilyAPI  by lazy {
            RetrofitContext.instance.create(FamilyAPI::class.java)
        }
    }
}
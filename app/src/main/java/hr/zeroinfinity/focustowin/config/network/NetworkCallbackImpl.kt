package hr.zeroinfinity.focustowin.config.network

import android.net.ConnectivityManager
import android.net.Network
import hr.zeroinfinity.focustowin.config.event.ConnectionEstablishedEvent
import hr.zeroinfinity.focustowin.config.event.ConnectionLostEvent
import hr.zeroinfinity.focustowin.config.event.EventBus

object NetworkCallbackImpl : ConnectivityManager.NetworkCallback() {

    override fun onLost(network: Network?) {
        super.onLost(network)

        EventBus.post(ConnectionLostEvent)
    }

    override fun onAvailable(network: Network?) {
        super.onAvailable(network)

        EventBus.post(ConnectionEstablishedEvent)
    }
}
package hr.zeroinfinity.focustowin.config.retrofit.api

import hr.zeroinfinity.focustowin.config.retrofit.RetrofitContext
import hr.zeroinfinity.focustowin.model.Chore
import hr.zeroinfinity.focustowin.model.Response
import io.reactivex.Observable
import retrofit2.http.*

interface ChoreAPI {

    @POST("/chore")
    fun create(@Body chore: Chore): Observable<Response<Long>>

    @PUT("/chore")
    fun update(@Body chore: Chore): Observable<Response<Long>>

    @DELETE("/chore")
    fun delete(@Query("id") id: Long): Observable<Response<Long>>

    companion object {
        val instance: ChoreAPI by lazy {
            RetrofitContext.instance.create(ChoreAPI::class.java)
        }
    }
}
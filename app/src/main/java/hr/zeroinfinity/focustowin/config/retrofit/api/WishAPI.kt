package hr.zeroinfinity.focustowin.config.retrofit.api

import hr.zeroinfinity.focustowin.config.retrofit.RetrofitContext
import hr.zeroinfinity.focustowin.model.Response
import hr.zeroinfinity.focustowin.model.Wish
import io.reactivex.Observable
import retrofit2.http.*

interface WishAPI {

    @POST("/wish")
    fun create(@Body wish: Wish): Observable<Response<Long>>

    @PUT("/wish")
    fun update(@Body wish: Wish): Observable<Response<Long>>

    @DELETE("/wish")
    fun delete(@Query("id") id: Long): Observable<Response<Long>>

    @GET("/wish/child")
    fun getByChildId(@Query("childId") childId: Long): Observable<List<Wish>>

    companion object {
        val instance: WishAPI by lazy {
            RetrofitContext.instance.create(WishAPI::class.java)
        }
    }
}
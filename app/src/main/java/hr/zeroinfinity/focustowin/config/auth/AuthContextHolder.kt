package hr.zeroinfinity.focustowin.config.auth

import android.content.SharedPreferences
import hr.zeroinfinity.focustowin.config.APP_MODE_SHARED_PREFERENCES
import hr.zeroinfinity.focustowin.config.AppMode
import hr.zeroinfinity.focustowin.config.CHILD_TOKEN_KEY_SHARED_PREFERENCES
import hr.zeroinfinity.focustowin.config.PARENT_TOKEN_KEY_SHARED_PREFERENCES
import hr.zeroinfinity.focustowin.config.gson.GsonContext

class AuthContextHolder private constructor(private val sharedPreferences: SharedPreferences) {
    private var parentToken: JwtTokenModel? = null
    private var childToken: Long? = null

    var mode: AppMode = AppMode.NONE
        private set

    init {
        mode = when (sharedPreferences.getInt(APP_MODE_SHARED_PREFERENCES, AppMode.NONE.ordinal)) {
            AppMode.CHILD.ordinal -> AppMode.CHILD
            AppMode.PARENT.ordinal -> AppMode.PARENT
            else -> AppMode.NONE
        }

        when (mode) {
            AppMode.PARENT -> {
                parentToken =
                    JwtTokenModel.fromJSON(sharedPreferences.getString(PARENT_TOKEN_KEY_SHARED_PREFERENCES, null))

                assert(parentToken != null)
            }
            AppMode.CHILD -> {
                childToken = sharedPreferences.getLong(CHILD_TOKEN_KEY_SHARED_PREFERENCES, -1L)

                assert(childToken != -1L)
            }
            else -> sharedPreferences.edit()
                .remove(CHILD_TOKEN_KEY_SHARED_PREFERENCES)
                .remove(PARENT_TOKEN_KEY_SHARED_PREFERENCES)
                .remove(APP_MODE_SHARED_PREFERENCES)
                .apply()
        }
    }

    fun setToken(_parentToken: JwtTokenModel) {
        assert(mode == AppMode.NONE)

        parentToken = _parentToken
        mode = AppMode.PARENT

        sharedPreferences.edit()
            .putString(PARENT_TOKEN_KEY_SHARED_PREFERENCES, GsonContext.instance.toJson(_parentToken))
            .putInt(APP_MODE_SHARED_PREFERENCES, AppMode.PARENT.ordinal)
            .remove(CHILD_TOKEN_KEY_SHARED_PREFERENCES)
            .apply()
    }

    fun setToken(_childToken: Long) {
        assert(mode == AppMode.NONE)

        childToken = _childToken
        mode = AppMode.CHILD

        sharedPreferences.edit()
            .putLong(CHILD_TOKEN_KEY_SHARED_PREFERENCES, _childToken)
            .putInt(APP_MODE_SHARED_PREFERENCES, AppMode.CHILD.ordinal)
            .remove(PARENT_TOKEN_KEY_SHARED_PREFERENCES)
            .apply()
    }

    fun removeToken() {
        parentToken = null
        childToken = null
        mode = AppMode.NONE

        sharedPreferences.edit()
            .remove(PARENT_TOKEN_KEY_SHARED_PREFERENCES)
            .remove(CHILD_TOKEN_KEY_SHARED_PREFERENCES)
            .putInt(APP_MODE_SHARED_PREFERENCES, AppMode.NONE.ordinal)
            .apply()
    }

    fun getAuthorizationHeader(): String {
        if (parentToken == null) {
            return BASIC_AUTH_HEADER
        }

        return "Bearer ${parentToken?.accessToken}"
    }

    fun getBasicAuthorizationHeader(): String {
        return BASIC_AUTH_HEADER
    }

    fun getRefreshToken(): String? {
        assert(mode == AppMode.PARENT)

        return parentToken?.refreshToken
    }

    fun getUserData(): Map<String, String>? {
        assert(mode == AppMode.PARENT)

        return parentToken?.userData
    }

    fun getChildId(): Long {
        assert(mode == AppMode.CHILD)

        return childToken!!
    }

    companion object {
        lateinit var instance: AuthContextHolder
        private const val BASIC_AUTH_HEADER = "Basic dXNlcjp1c2Vy"

        fun init(sharedPreferences: SharedPreferences) {
            instance = AuthContextHolder(sharedPreferences)
        }
    }
}
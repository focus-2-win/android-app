package hr.zeroinfinity.focustowin.config.network

typealias Retryable = () -> Unit

class RequestFailure(val retryable: Retryable, val message: String)
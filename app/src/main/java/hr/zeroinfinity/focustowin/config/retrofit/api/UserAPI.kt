package hr.zeroinfinity.focustowin.config.retrofit.api

import hr.zeroinfinity.focustowin.config.auth.JwtTokenModel
import hr.zeroinfinity.focustowin.config.retrofit.RetrofitContext
import hr.zeroinfinity.focustowin.model.Response
import hr.zeroinfinity.focustowin.model.User
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*

interface UserAPI {

    @POST("/user")
    fun signUp(@Body user: User): Observable<Response<Long>>

    @GET("/user/forgotPassword")
    fun resetPassword(@Query("email") email: String): Observable<Response<Long>>

    @FormUrlEncoded
    @POST("/oauth/token")
    fun refreshToken(
        @Field("grant_type") grantType: String = "refresh_token",
        @Field("refresh_token") refreshToken: String
    ): Call<JwtTokenModel>


    @FormUrlEncoded
    @POST("/oauth/token")
    fun signIn(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String = "password"
    ): Call<JwtTokenModel>

    companion object {
        val instance: UserAPI by lazy {
            RetrofitContext.instance.create(UserAPI::class.java)
        }
    }
}
package hr.zeroinfinity.focustowin.config.auth

import android.os.AsyncTask
import hr.zeroinfinity.focustowin.common.ICallback
import hr.zeroinfinity.focustowin.config.retrofit.api.UserAPI
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

object AuthCallExecutor {

    fun refreshToken(): Boolean {
        val refreshToken = AuthContextHolder.instance.getRefreshToken() ?: return false

        return resolveSyncCall(UserAPI.instance.refreshToken(refreshToken = refreshToken))
    }

    fun refreshToken(callback: ICallback<Boolean>) {
        val refreshToken = AuthContextHolder.instance.getRefreshToken()
        if (refreshToken == null) {
            callback.call(false)
            return
        }

        AsyncTask.execute {
            resolveAsyncCall(UserAPI.instance.refreshToken(refreshToken = refreshToken), callback)
        }
    }

    fun signIn(username: String, password: String): Boolean {
        return resolveSyncCall(UserAPI.instance.signIn(username, password))
    }

    fun signIn(username: String, password: String, callback: ICallback<Boolean>) {
        AsyncTask.execute {
            resolveAsyncCall(UserAPI.instance.signIn(username, password), callback)
        }
    }

    private fun resolveAsyncCall(call: Call<JwtTokenModel>, callback: ICallback<Boolean>) {
        call.enqueue(object : Callback<JwtTokenModel> {
            override fun onFailure(call: Call<JwtTokenModel>, t: Throwable) {
                AuthContextHolder.instance.removeToken()
                callback.call(false)
            }

            override fun onResponse(call: Call<JwtTokenModel>, response: Response<JwtTokenModel>) {
                if (response.code() != 200) {
                    callback.call(false)
                    return
                }

                val respBody = response.body()
                AuthContextHolder.instance.setToken(respBody!!)

                callback.call(true)
            }
        })
    }

    private fun resolveSyncCall(call: Call<JwtTokenModel>): Boolean {
        return try {
            val resp = call.execute()
            val respBody = resp.body()
            AuthContextHolder.instance.setToken(respBody!!)
            true
        } catch (e: IOException) {
            AuthContextHolder.instance.removeToken()
            false
        }
    }
}
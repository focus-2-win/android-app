package hr.zeroinfinity.focustowin.config.event

import hr.zeroinfinity.focustowin.common.pattern.Listener
import hr.zeroinfinity.focustowin.common.pattern.SubjectAdapter

object EventBus {
    private val subject = SubjectAdapter<Event>()

    fun register(eventListener: EventListener) = subject.addListener(eventListener)

    fun unregister(eventListener: EventListener) = subject.removeListener(eventListener)

    fun post(event: Event) = subject.notifyListeners(event)

    interface EventListener : Listener<Event>
}
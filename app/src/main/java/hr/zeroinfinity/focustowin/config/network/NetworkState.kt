package hr.zeroinfinity.focustowin.config.network

enum class NetworkState {
    LOADING, LOADED, FAILED
}

package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.FamilyAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_post_sign_up.*

class PostSignUpActivity : AuthBaseActivity(false) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_sign_up)

        logoutButton.setOnClickListener {
            AuthContextHolder.instance.removeToken()
            SignInActivity.start(this@PostSignUpActivity, true)
        }

        joinButton.setOnClickListener {
            QRPreviewActivity.start(
                this,
                false,
                AuthContextHolder.instance.getUserData()?.get("id")?.toLong()
            )
        }

        createButton.setOnClickListener {
            createFamily()
        }
    }

    private fun createFamily() {
        FamilyAPI.instance.createFamily()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("User ${AuthContextHolder.instance.getUserData()?.get("id")} created a family.")

                    ParentMainActivity.start(this@PostSignUpActivity, true)
                },
                {
                    F2WLogger.error(it.message)

                    Snackbar.make(rootView, getString(R.string.no_internet_error_message), Snackbar.LENGTH_SHORT)
                        .setAction(getString(R.string.retry_label)) { createFamily() }
                        .show()
                }
            )
    }

    companion object {

        fun start(baseActivity: Activity, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseActivity, PostSignUpActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }
}

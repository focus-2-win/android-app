package hr.zeroinfinity.focustowin.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.Chore
import hr.zeroinfinity.focustowin.model.ChoreStatus

class ChoirReviewListAdapter(context: Context, chores: List<Chore>, private val isChild: Boolean = false) :
    ArrayAdapter<Chore>(context, R.layout.chore_review_list_item, chores) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.chore_review_list_item, parent, false)
        }

        val chore = getItem(position)

        val choreTextView = view?.findViewById<TextView>(R.id.choreTextView)
        val difficulty = view?.findViewById<ImageView>(R.id.difficultyImageView)
        val statusCheckBox = view?.findViewById<CheckBox>(R.id.statusCheckBox)
        val chorePointsTextView = view?.findViewById<TextView>(R.id.choresPointsTextView)

        choreTextView?.text = chore?.name

        statusCheckBox?.isChecked = chore.status == ChoreStatus.COMPLETED
        if (isChild) {
            statusCheckBox?.setOnTouchListener { _, _ -> true }

            chorePointsTextView?.text = context.getString(R.string.points_format, chore?.points!!)
            chorePointsTextView?.visibility = View.VISIBLE
            difficulty?.visibility = View.GONE

        } else {
            statusCheckBox?.isEnabled = true
            statusCheckBox?.setOnClickListener {
                chore?.status = if (statusCheckBox.isChecked) ChoreStatus.COMPLETED else ChoreStatus.UNCOMPLETED
            }

            when {
                chore?.points in 50..100 -> {
                    difficulty?.setImageResource(R.drawable.ic_star_easy)
                }
                chore?.points in 100..200 -> {
                    difficulty?.setImageResource(R.drawable.ic_star_medium)
                }
                else -> {
                    difficulty?.setImageResource(R.drawable.ic_star_hard)
                }
            }
        }

        return view!!
    }
}
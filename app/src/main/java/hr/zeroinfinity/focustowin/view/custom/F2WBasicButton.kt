package hr.zeroinfinity.focustowin.view.custom

import android.content.Context
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import hr.zeroinfinity.focustowin.R

class F2WBasicButton : AppCompatButton {

    constructor(context: Context) : super(context) {
        initGUI()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initGUI()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        initGUI()
    }

    private fun initGUI() {
        typeface = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.resources.getFont(hr.zeroinfinity.focustowin.R.font.poppins_bold)
        } else {
            ResourcesCompat.getFont(context, hr.zeroinfinity.focustowin.R.font.poppins_bold)
        }

        setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        textAlignment = View.TEXT_ALIGNMENT_CENTER
        setTextColor(context.resources.getColor(android.R.color.white))

        background = context.resources.getDrawable(R.drawable.basic_btn)
    }

}

package hr.zeroinfinity.focustowin.view.custom

import android.content.Context
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View


class F2WMessageTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {
        resolveTypeface()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        resolveTypeface()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        resolveTypeface()
    }

    private fun resolveTypeface() {
        typeface = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.resources.getFont(hr.zeroinfinity.focustowin.R.font.poppins_bold)
        } else {
            ResourcesCompat.getFont(context, hr.zeroinfinity.focustowin.R.font.poppins_bold)
        }

        textAlignment = View.TEXT_ALIGNMENT_CENTER
    }
}
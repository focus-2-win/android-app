package hr.zeroinfinity.focustowin.view.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import hr.zeroinfinity.focustowin.model.User

class SignUpViewModel : ViewModel() {
    val user = MutableLiveData<User>()
    val done = MutableLiveData<Boolean>()

    init {
        user.value = User()
    }
}
package hr.zeroinfinity.focustowin.view.custom

import android.content.Context
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import hr.zeroinfinity.focustowin.R

class F2WTitleTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {
        resolveTypeface()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        resolveTypeface()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        resolveTypeface()
    }

    private fun resolveTypeface() {
        typeface = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.resources.getFont(R.font.poppins_bold)
        } else {
            ResourcesCompat.getFont(context, R.font.poppins_bold)
        }

        setTextSize(TypedValue.COMPLEX_UNIT_SP, 24f)
        textAlignment = View.TEXT_ALIGNMENT_CENTER
    }
}
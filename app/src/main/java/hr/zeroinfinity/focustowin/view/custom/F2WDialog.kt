package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import hr.zeroinfinity.focustowin.R
import kotlinx.android.synthetic.main.dialog_custom.*

class F2WDialog private constructor(
    context: Context,
    private val titleText: String,
    private var messageText: String,
    private val positiveBtnText: String,
    private val negativeBtnText: String?,
    private val positiveBtnListener: View.OnClickListener?,
    private val negativeBtnListener: View.OnClickListener?,
    private val canceledOnTouch: Boolean,
    private val bothPositive: Boolean

) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_custom)
        setCanceledOnTouchOutside(canceledOnTouch)

        titleTextView.text = titleText
        messageTextView.text = messageText

        positiveButton.text = positiveBtnText
        positiveButton.setOnClickListener {
            dismiss()
            positiveBtnListener?.onClick(it)
        }

        if (negativeBtnText == null) {
            negativeButton.visibility = View.GONE
        } else {
            negativeButton.text = negativeBtnText
            negativeButton.setOnClickListener {
                dismiss()
                negativeBtnListener?.onClick(it)
            }

            if (bothPositive) {
                negativeButton.background = context.getDrawable(R.drawable.positive_btn)
            }
        }
    }

    class Builder(private val context: Context) {
        private var title: String = ""
        private var message: String = ""
        private var positiveBtnText: String = "OK"
        private var negativeBtnText: String? = null
        private var positiveBtnListener: View.OnClickListener? = null
        private var negativeBtnListener: View.OnClickListener? = null
        private var cot: Boolean = false
        private var bp: Boolean = false

        fun setTitle(_title: String): Builder {
            title = _title
            return this
        }

        fun setMessage(_message: String): Builder {
            message = _message
            return this
        }

        fun setPositiveButtonText(_text: String): Builder {
            positiveBtnText = _text
            return this
        }

        fun setNegativeButtonText(_text: String): Builder {
            negativeBtnText = _text
            return this
        }

        fun setPositiveButtonListener(_listener: View.OnClickListener): Builder {
            positiveBtnListener = _listener
            return this
        }

        fun setNegativeButtonListener(_listener: View.OnClickListener): Builder {
            negativeBtnListener = _listener
            return this
        }

        fun setCanceledOnTouchOutside(_cot: Boolean): Builder {
            cot = _cot
            return this
        }

        fun bothPositive(_bp: Boolean): Builder {
            bp = _bp
            return this
        }

        fun build(): F2WDialog {
            return F2WDialog(
                context,
                title,
                message,
                positiveBtnText,
                negativeBtnText,
                positiveBtnListener,
                negativeBtnListener,
                cot,
                bp
            )
        }
    }

    companion object {
        fun builder(context: Context): Builder {
            return Builder(context)
        }
    }
}
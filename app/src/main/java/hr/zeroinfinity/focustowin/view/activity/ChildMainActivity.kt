package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.view.WindowManager
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.FamilyAPI
import hr.zeroinfinity.focustowin.view.fragment.ApplicationInAlphaFragment
import hr.zeroinfinity.focustowin.view.fragment.ChildMainFragment
import hr.zeroinfinity.focustowin.view.viewmodel.ChildMainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_child_main.*
import retrofit2.HttpException

class ChildMainActivity : AuthBaseActivity(true), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var sharedViewModel: ChildMainViewModel
    private var currentNavItemId: Int = R.id.navigation_family
    private var currentFragmentIndex: Int = 0

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        if (currentNavItemId == item.itemId) return@OnNavigationItemSelectedListener false

        currentNavItemId = item.itemId
        var fragment: Fragment? = null

        val previousFragmentIndex = currentFragmentIndex
        when (item.itemId) {
            R.id.navigation_dashboard -> {
                fragment = ChildMainFragment.newInstance()
                currentFragmentIndex = 0
            }
            R.id.navigation_leaderboard -> {
                fragment = ApplicationInAlphaFragment.newInstance()
                currentFragmentIndex = 1
            }
        }

        loadFragment(fragment, currentFragmentIndex > previousFragmentIndex)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_child_main)


        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        rootView.setOnRefreshListener(this)

        onRefresh()
    }

    override fun onRefresh() {
        rootView.isRefreshing = false

        loadingOverlay.show()
        FamilyAPI.instance.getFamily(AuthContextHolder.instance.getChildId())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("Family ${it.id} fetched.")

                    sharedViewModel = ViewModelProviders.of(this@ChildMainActivity).get(ChildMainViewModel::class.java)
                    sharedViewModel.family.postValue(it)

                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, ChildMainFragment.newInstance())
                        .commit()

                    loadingOverlay.dismiss()
                },
                {
                    loadingOverlay.dismiss()
                    if (it is HttpException && it.code() == 404) {
                        F2WLogger.info("Child ${AuthContextHolder.instance.getChildId()} doesn't have family.")

                        QRPreviewActivity.start(baseActivity = this@ChildMainActivity, isChild = true, asNewTask = true)
                    } else {
                        F2WLogger.error(it.message)

                        Snackbar.make(
                            rootView,
                            getString(R.string.no_internet_error_message),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.retry_label) {
                                onRefresh()
                            }
                            .show()
                    }
                }
            )
    }


    private fun loadFragment(fragment: Fragment?, slideLeft: Boolean): Boolean {
        var anim1 = R.anim.slide_in_left
        var anim2 = R.anim.slide_out_right
        if (slideLeft) {
            anim1 = R.anim.slide_in_right
            anim2 = R.anim.slide_out_left
        }

        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(anim1, anim2)
                .replace(R.id.fragmentContainer, fragment)
                .commit()
            return true
        }
        return false
    }

    companion object {

        fun start(baseActivity: Activity, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseActivity, ChildMainActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }
}

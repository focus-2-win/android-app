package hr.zeroinfinity.focustowin.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.SurfaceHolder
import android.view.View
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.config.FAMILY_ID_INTENT_CODE
import hr.zeroinfinity.focustowin.config.IS_CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.config.PARENT_INTENT_CODE
import hr.zeroinfinity.focustowin.config.retrofit.api.FamilyAPI
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.Request
import hr.zeroinfinity.focustowin.view.custom.F2WCreateChildDialog
import hr.zeroinfinity.focustowin.view.custom.F2WDialog
import hr.zeroinfinity.focustowin.view.custom.F2WDialogWithInput
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_qr_scan.*
import retrofit2.HttpException


class QRScanActivity : AuthBaseActivity(false), Detector.Processor<Barcode>, SurfaceHolder.Callback2 {

    private lateinit var detector: BarcodeDetector
    private lateinit var cameraSource: CameraSource
    private var isChildMode: Boolean = false
    private var familyId: Long = 0L
    private var id: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr_scan)

        intent.let {
            isChildMode = it.getBooleanExtra(IS_CHILD_INTENT_CODE, false)
            familyId = it.getLongExtra(FAMILY_ID_INTENT_CODE, 0L)
        }

        detector = BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build()

        cameraSource = CameraSource.Builder(this, detector)
            .setRequestedPreviewSize(1080, 1080)
            .setRequestedFps(30f)
            .setAutoFocusEnabled(true)
            .build()
        surfaceViewCamera.holder.addCallback(this)

        F2WDialog.builder(this)
            .setTitle(getString(R.string.instructions_title))
            .setMessage(
                if (isChildMode) getString(R.string.add_child_instructions_text)
                else getString(R.string.add_parent_instructions_text)
            )
            .setPositiveButtonListener(View.OnClickListener {
                detector.setProcessor(this)
            })
            .build()
            .show()

        enterCodeManuallyButton.setOnClickListener { enterCodeManually() }
    }

    override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
        val barCodes = detections?.detectedItems
        if (barCodes?.size()!! <= 0) return

        val scannedId = barCodes.valueAt(0).displayValue?.toLongOrNull()
        if (scannedId == null || id != null) return

        id = scannedId

        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v.vibrate(200)
        }

        runOnUiThread { submitCode(id!!) }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        cameraSource.stop()
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        if (ContextCompat.checkSelfPermission(
                this@QRScanActivity,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            cameraSource.start(holder)
        } else {
            ActivityCompat.requestPermissions(this@QRScanActivity, arrayOf(Manifest.permission.CAMERA), 123)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != 123) return
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            cameraSource.start(surfaceViewCamera.holder)
        } else {
            F2WDialogWithInput.builder(this@QRScanActivity)
                .setTitle(getString(R.string.hard_way_title))
                .setMessage(getString(R.string.enter_code_manually_message))
                .setPositiveButtonText(getString(R.string.submit_label))
                .setNegativeButtonText(getString(R.string.dismiss_label))
                .setListener { code, isSubmitted ->
                    if (isSubmitted) submitCode(code.toLong())
                    else finish()
                }
                .setCanceledOnTouchOutside(false)
                .build()
                .show()
        }
    }

    override fun release() = Unit
    override fun surfaceRedrawNeeded(holder: SurfaceHolder?) = Unit
    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) = Unit

    private fun submitCode(code: Long) {
        if (isChildMode) {
            F2WCreateChildDialog.builder(this)
                .setListener { nick, avatarId ->
                    val child = Child(
                        code,
                        nick,
                        avatarId,
                        null,
                        null,
                        null
                    )

                    addChildToFamily(child)
                }
                .setDismissListener {
                    finish()
                }
                .build()
                .show()
        } else {
            addParentToFamily(code)
        }
    }

    private fun addParentToFamily(code: Long) {
        loadingOverlay.show()
        FamilyAPI.instance.addParent(Request(code))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    val intent = Intent()
                    intent.putExtra(PARENT_INTENT_CODE, it)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                },
                {
                    loadingOverlay.dismiss()
                    if (it is HttpException && it.code() == 404) {
                        Snackbar.make(
                            rootView,
                            getString(R.string.adding_parent_error_message),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.cancel_label) { finish() }
                            .show()
                    } else {
                        Snackbar.make(
                            rootView,
                            getString(R.string.no_internet_error_message),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.retry_label) { addParentToFamily(code) }
                            .show()
                    }
                }
            )
    }

    private fun addChildToFamily(child: Child) {
        loadingOverlay.show()
        FamilyAPI.instance.addChild(child)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    loadingOverlay.dismiss()
                    val intent = Intent()
                    intent.putExtra(CHILD_INTENT_CODE, it)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                },
                {
                    loadingOverlay.dismiss()
                    if (it is HttpException && it.code() == 404) {
                        Snackbar.make(
                            rootView,
                            getString(R.string.adding_child_to_family_error),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.cancel_label) { finish() }
                            .show()
                    } else {
                        Snackbar.make(
                            rootView,
                            getString(R.string.no_internet_error_message),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.retry_label) { addChildToFamily(child) }
                            .show()
                    }
                })
    }

    private fun enterCodeManually() {
        F2WDialogWithInput.builder(this@QRScanActivity)
            .setTitle(getString(R.string.hard_way_title))
            .setMessage(getString(R.string.enter_code_manually_message))
            .setPositiveButtonText(getString(R.string.submit_label))
            .setNegativeButtonText(getString(R.string.dismiss_label))
            .setListener { code, isSubmitted ->
                if (isSubmitted) {
                    val c = code.toLongOrNull()
                    if (c == null) {
                        Snackbar.make(rootView, getString(R.string.code_format_error), Snackbar.LENGTH_SHORT)
                            .setAction(getString(R.string.retry_label)) { enterCodeManually() }
                            .show()
                    } else {
                        submitCode(c)
                    }
                }
            }
            .build()
            .show()
    }


    companion object {

        fun start(
            baseActivity: Activity,
            isChild: Boolean,
            familyId: Long,
            requestCode: Int? = null,
            asNewTask: Boolean = false
        ) {
            val intent = Intent(baseActivity, QRScanActivity::class.java)
            intent.putExtra(IS_CHILD_INTENT_CODE, isChild)
            intent.putExtra(FAMILY_ID_INTENT_CODE, familyId)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }

        fun start(
            baseFragment: Fragment,
            isChild: Boolean,
            familyId: Long,
            requestCode: Int? = null,
            asNewTask: Boolean = false
        ) {
            val intent = Intent(baseFragment.context, QRScanActivity::class.java)
            intent.putExtra(IS_CHILD_INTENT_CODE, isChild)
            intent.putExtra(FAMILY_ID_INTENT_CODE, familyId)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseFragment.startActivity(intent)
            } else {
                baseFragment.startActivityForResult(intent, requestCode)
            }
        }
    }
}

package hr.zeroinfinity.focustowin.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.Refreshable
import hr.zeroinfinity.focustowin.model.internal.Avatar

class AvatarSelectorAdapter : RecyclerView.Adapter<AvatarSelectorAdapter.ViewHolder>(), Refreshable<Avatar> {

    private val avatars = Avatar.ALL.map {
        if (it.id == 1) {
            AvatarViewModel(it, true)
        } else {
            AvatarViewModel(it, false)
        }
    }

    var selectedAvatarId = 1
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.avatar_list_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(avatars[position])
    }

    override fun getItemCount(): Int {
        return avatars.size
    }

    override fun refresh(newData: Avatar) {
        avatars.forEach { it.selected = false }
        avatars.find { it.avatar.id == newData.id }?.selected = true
        selectedAvatarId = newData.id

        notifyDataSetChanged()
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private var imageView = view.findViewById<ImageView>(R.id.avatarImageView)

        fun bind(value: AvatarViewModel) {
            imageView.setImageDrawable(value.avatar.getDrawable(view.context))
            view.alpha = if (value.selected) 1f else 0.25f

            view.setOnClickListener {
                refresh(value.avatar)
            }
        }
    }

    data class AvatarViewModel(val avatar: Avatar, var selected: Boolean)
}
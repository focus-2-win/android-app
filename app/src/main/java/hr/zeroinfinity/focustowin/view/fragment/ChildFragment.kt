package hr.zeroinfinity.focustowin.view.fragment


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.internal.Avatar
import kotlinx.android.synthetic.main.fragment_child.*

class ChildFragment : Fragment() {

    var child: MutableLiveData<Child?> = MutableLiveData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            child.postValue(it.getParcelable(CHILD_INTENT_CODE))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_child, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        child.observe(this, Observer {
            if (it == null) return@Observer

            val avatar = Avatar.ALL.firstOrNull { avatar -> avatar.id == it.avatarId!! }
            avatarImageView.setImageResource(
                avatar?.resId ?: R.drawable.avatar1
            )

            nameTextView.text = it.username
            view.requestLayout()
        })
    }

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)

        args.let {
            child.postValue(it?.getParcelable(CHILD_INTENT_CODE))
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(child: Child) =
            ChildFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(CHILD_INTENT_CODE, child)
                }
            }
    }
}

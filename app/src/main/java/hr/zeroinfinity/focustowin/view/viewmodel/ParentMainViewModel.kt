package hr.zeroinfinity.focustowin.view.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import hr.zeroinfinity.focustowin.model.Family

class ParentMainViewModel : ViewModel() {

    val family = MutableLiveData<Family>()
}
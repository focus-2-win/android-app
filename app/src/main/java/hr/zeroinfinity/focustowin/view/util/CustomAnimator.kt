package hr.zeroinfinity.focustowin.view.util

import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.View

object CustomAnimator {

    fun fade(view: View, duration: Long, from: Float, to: Float, listener: Animator.AnimatorListener? = null) {
        assert(from in 0f..1f && to in 0f..1f)

        val animator = ObjectAnimator.ofFloat(view, View.ALPHA, from, to)
        animator.duration = duration
        if (listener != null) animator.addListener(listener)

        animator.start()
    }
}
package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.EditText
import hr.zeroinfinity.focustowin.R
import kotlinx.android.synthetic.main.dialog_custom.messageTextView
import kotlinx.android.synthetic.main.dialog_custom.negativeButton
import kotlinx.android.synthetic.main.dialog_custom.positiveButton
import kotlinx.android.synthetic.main.dialog_custom.titleTextView
import kotlinx.android.synthetic.main.dialog_with_2_inputs_custom.*

typealias DialogWith2InputsListener = (String, String, Boolean) -> Unit

class F2WDialogWith2Inputs private constructor(
    context: Context,
    private val titleText: String,
    private val messageText: String,
    private val input1Placeholder: String,
    private val input1Optional: Boolean,
    private val input2Placeholder: String,
    private val input2Optional: Boolean,
    private val positiveBtnText: String,
    private val negativeBtnText: String?,
    private val listener: DialogWith2InputsListener,
    private val canceledOnTouch: Boolean

) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_with_2_inputs_custom)
        setCanceledOnTouchOutside(canceledOnTouch)

        titleTextView.text = titleText
        messageTextView.text = messageText

        input.hint = input1Placeholder
        input2.hint = input2Placeholder

        positiveButton.text = positiveBtnText
        positiveButton.setOnClickListener {
            if (validate(input, input2)) {
                listener.invoke(input.text.toString(), input2.text.toString(), true)
                dismiss()
            }
        }

        if (negativeBtnText == null) {
            negativeButton.visibility = View.GONE
        } else {
            negativeButton.text = negativeBtnText
            negativeButton.setOnClickListener {
                listener.invoke(input.text.toString(), input2.text.toString(), false)
                dismiss()
            }
        }
    }

    private fun validate(input: EditText?, input2: EditText?): Boolean {
        var ret = true

        if (!input1Optional && input?.text.isNullOrEmpty()) {
            input?.error = context.getString(R.string.error_required_field)
            ret = false
        }

        if (!input2Optional && input2?.text.isNullOrEmpty()) {
            input?.error = context.getString(R.string.error_required_field)
            ret = false
        }

        return ret
    }

    override fun onBackPressed() {
        super.onBackPressed()

        listener.invoke(input.text.toString(), input2.text.toString(), false)
    }

    class Builder(private val context: Context) {
        private var title: String = ""
        private var message: String = ""
        private var input1Placeholder: String = ""
        private var input1Optional: Boolean = false
        private var input2Placeholder: String = ""
        private var input2Optional: Boolean = false
        private var positiveBtnText: String = "OK"
        private var negativeBtnText: String? = null
        private var listener: DialogWith2InputsListener? = null
        private var cot: Boolean = true

        fun setTitle(_title: String): Builder {
            title = _title
            return this
        }

        fun setMessage(_message: String): Builder {
            message = _message
            return this
        }

        fun setPositiveButtonText(_text: String): Builder {
            positiveBtnText = _text
            return this
        }

        fun setNegativeButtonText(_text: String): Builder {
            negativeBtnText = _text
            return this
        }

        fun setListener(_listener: DialogWith2InputsListener): Builder {
            listener = _listener
            return this
        }

        fun setCanceledOnTouchOutside(_cot: Boolean): Builder {
            cot = _cot
            return this
        }

        fun setInput1Placeholder(_input1Placeholder: String): Builder {
            input1Placeholder = _input1Placeholder
            return this
        }

        fun setInput2Placeholder(_input2Placeholder: String): Builder {
            input2Placeholder = _input2Placeholder
            return this
        }

        fun setInput1Optional(_input1Optional: Boolean): Builder {
            input1Optional = _input1Optional
            return this
        }

        fun setInput2Optional(_input2Optional: Boolean): Builder {
            input2Optional = _input2Optional
            return this
        }

        fun build(): F2WDialogWith2Inputs {
            return F2WDialogWith2Inputs(
                context,
                title,
                message,
                input1Placeholder,
                input1Optional,
                input2Placeholder,
                input2Optional,
                positiveBtnText,
                negativeBtnText,
                listener!!,
                cot
            )
        }
    }

    companion object {
        fun builder(context: Context): Builder {
            return Builder(context)
        }
    }
}
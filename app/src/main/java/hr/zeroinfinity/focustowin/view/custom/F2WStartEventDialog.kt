package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.view.adapter.ChoirReviewListAdapter
import kotlinx.android.synthetic.main.dialog_start_event.*

typealias OnStartEvent = (Event) -> Unit

class F2WStartEventDialog(context: Context, private val event: Event, private val onStartEvent: OnStartEvent) :
    Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparentHalf)))
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        setContentView(R.layout.dialog_start_event)

        durationTextView.text =
            context.getString(R.string.leave_your_phone_for_s_minutes_format, getDuration(event.duration!!))

        if (event.chores == null || event.chores?.size == 0) {
            choresListView.visibility = View.GONE
            choresMessageTextView.visibility = View.GONE
        } else {
            val params = choresListView.layoutParams
            params.height = if (event.chores?.size!! < 4) {
                event.chores?.size!! * 55
            } else {
                130
            }

            choresListView.layoutParams = params
            choresListView.invalidate()

            choresListView.adapter = ChoirReviewListAdapter(context, event.chores!!, true)
        }


        positiveButton.setOnClickListener {
            onStartEvent.invoke(event)
            dismiss()
        }

        negativeButton.setOnClickListener {
            dismiss()
        }
    }

    override fun onBackPressed() {
        dismiss()
    }

    private fun getDuration(duration: Int): String =
        if (duration < 60) {
            context.getString(R.string.minutes_format, duration)
        } else {
            context.getString(R.string.hours_format, String.format("%.1f", duration.toDouble() / 60))
        }

}
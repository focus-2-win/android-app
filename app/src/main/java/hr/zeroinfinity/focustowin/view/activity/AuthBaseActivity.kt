package hr.zeroinfinity.focustowin.view.activity

import android.app.AlertDialog
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.event.Event
import hr.zeroinfinity.focustowin.config.event.UnauthorizedEvent

abstract class AuthBaseActivity(private val isForChild: Boolean) : BaseActivity() {

    override fun update(data: Event) {
        super.update(data)

        when (data) {
            is UnauthorizedEvent -> {
                if (isForChild) {
                    runOnUiThread {
                        AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(getString(R.string.session_expired_label))
                            .setMessage(getString(R.string.account_no_longer_active_message))
                            .setNegativeButton(getString(R.string.exit_label)) { _, _ ->
                                moveTaskToBack(true)
                            }
                            .setPositiveButton(getString(R.string.sign_in_label)) { _, _ ->
                                run {
                                    QRPreviewActivity.start(
                                        this,
                                        true,
                                        AuthContextHolder.instance.getChildId(),
                                        asNewTask = true
                                    )
                                }
                            }
                            .show()
                    }
                } else {
                    runOnUiThread {
                        AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(getString(R.string.session_expired_label))
                            .setMessage(getString(R.string.sign_in_required_warning))
                            .setNegativeButton(getString(R.string.exit_label)) { _, _ ->
                                AuthContextHolder.instance.removeToken()
                                moveTaskToBack(true)
                            }
                            .setPositiveButton(getString(R.string.sign_in_label)) { _, _ ->
                                run {
                                    AuthContextHolder.instance.removeToken()
                                    SignInActivity.start(this, true)
                                }
                            }
                            .show()
                    }
                }
            }
        }


    }
}
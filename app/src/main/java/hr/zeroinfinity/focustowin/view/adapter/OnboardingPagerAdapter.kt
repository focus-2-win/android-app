package hr.zeroinfinity.focustowin.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import hr.zeroinfinity.focustowin.view.fragment.OnboardingFragment

class OnboardingPagerAdapter(fragmentManager: FragmentManager, private val isChild: Boolean) :
    FragmentPagerAdapter(fragmentManager) {

    override fun getItem(index: Int): Fragment {
        return OnboardingFragment.newInstance(index, isChild)
    }

    override fun getCount(): Int = if (isChild) 3 else 4
}
package hr.zeroinfinity.focustowin.view.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.Refreshable
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.model.Wish
import hr.zeroinfinity.focustowin.model.WishStatus


typealias OnWishSelected = (Wish) -> Unit

class WishListAdapter(
    private val wishes: MutableList<Wish>,
    private val onClickListener: OnWishSelected? = null,
    private val onLongClickListener: OnWishSelected? = null,
    private var selectedWish: Wish? = null
) :
    RecyclerView.Adapter<WishListAdapter.ViewHolder>(),
    Refreshable<List<Wish>> {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.wish_list_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(wishes[position])
    }

    override fun getItemCount(): Int {
        return wishes.size
    }

    override fun refresh(newData: List<Wish>) {
        wishes.clear()
        wishes.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val imageView = view.findViewById<ImageView>(R.id.wishImageView)
        private val statusImageView = view.findViewById<ImageView>(R.id.wishStatusImageView)
        private val wishDescriptionTextView = view.findViewById<TextView>(R.id.wishDescriptionTextView)
        private val wishPointsTextView = view.findViewById<TextView>(R.id.wishPointsTextView)
        private val selectedOverlay = view.findViewById<CardView>(R.id.selectedOverlay)

        fun bind(value: Wish) {
            wishDescriptionTextView.text = value.description

            if (value.imageUrl.isNullOrBlank()) {
                imageView.setImageResource(R.drawable.ic_star_border_black_24dp)
            } else {
                DownloadImageTask(imageView).execute(value.imageUrl)
            }

            when (value.status?.id) {
                WishStatus.PENDING.id -> statusImageView.setImageResource(R.drawable.ic_status_not_accepted)
                WishStatus.ACCEPTED.id -> {
                    wishPointsTextView.text = view.context.getString(
                        R.string.points_points_format, value.currentPoints, value.targetPoints
                    )
                    statusImageView.setImageResource(R.drawable.ic_status_accepted)
                }
            }

            if (onClickListener != null) {
                view.setOnClickListener {
                    if (selectedWish != null) {
                        selectedWish = value
                        refresh(wishes.toMutableList())
                    }

                    onClickListener.invoke(value)
                }
            }
            if (onLongClickListener != null) view.setOnLongClickListener {
                onLongClickListener.invoke(value)
                true
            }

            if (selectedWish != null) {
                if (selectedWish == value) {
                    selectedOverlay.visibility = View.VISIBLE
                } else {
                    selectedOverlay.visibility = View.INVISIBLE
                }
            }
        }
    }

    private class DownloadImageTask(var bmImage: ImageView?) : AsyncTask<String, Void, Bitmap>() {

        override fun onCancelled(result: Bitmap?) {
            super.onCancelled(result)

            bmImage = null
        }

        override fun doInBackground(vararg urls: String): Bitmap? {
            val urlDisplay = urls[0]
            var mIcon: Bitmap? = null
            try {
                val inputStream = java.net.URL(urlDisplay).openStream()
                mIcon = BitmapFactory.decodeStream(inputStream)
            } catch (e: Exception) {
                F2WLogger.warn(e.message)
            }

            return mIcon
        }

        override fun onPostExecute(result: Bitmap?) {
            if (result != null) bmImage?.setImageBitmap(result)
            else bmImage?.setImageResource(R.drawable.ic_star_border_black_24dp)
            bmImage = null
        }
    }
}
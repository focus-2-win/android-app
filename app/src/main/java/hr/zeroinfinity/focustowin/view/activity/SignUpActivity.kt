package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Patterns
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.ICallback
import hr.zeroinfinity.focustowin.config.auth.AuthCallExecutor
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.UserAPI
import hr.zeroinfinity.focustowin.view.adapter.SignUpPagerAdapter
import hr.zeroinfinity.focustowin.view.viewmodel.SignUpViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity() {

    private var page = 0
    private lateinit var sharedViewModel: SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        tabDots.setupWithViewPager(mViewPager, true)
        mViewPager.adapter = SignUpPagerAdapter(supportFragmentManager)

        sharedViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        sharedViewModel.user.observe(this, Observer { user ->
            user?.let {
                when (page) {
                    0 -> nextButton.isEnabled = !it.name.isNullOrBlank()
                    1 -> nextButton.isEnabled = !it.surname.isNullOrBlank()
                    2 -> nextButton.isEnabled = it.email.isValidEmail()
                    3 -> nextButton.isEnabled = it.password.isValidPassword()
                }
            }
        })

        sharedViewModel.done.observe(this, Observer { onNext() })

        nextButton.setOnClickListener {
            onNext()
        }

        previousButton.setOnClickListener {
            onPrevious()
        }

        setHideSoftKeyboardOnTouch(rootView, this)
    }

    private fun onNext() {
        if (page == 3) {
            handleSubmit()
            return
        }

        page++
        if (page == 3) {
            nextButton.text = getString(R.string.submit_label)
        }

        if (page == 1) {
            previousButton.text = getString(R.string.previous_label)
        }

        mViewPager.post {
            mViewPager.setCurrentItem(page, true)
            sharedViewModel.user.postValue(sharedViewModel.user.value)
        }
    }

    private fun onPrevious() {
        if (page == 0) {
            finish()
            return
        }

        page--
        if (page == 2) {
            nextButton.text = getString(R.string.next_label)
        }

        if (page == 0) {
            previousButton.text = getString(R.string.back_label)
        }

        mViewPager.post {
            mViewPager.setCurrentItem(page, true)
            sharedViewModel.user.postValue(sharedViewModel.user.value)
        }
    }

    private fun handleSubmit() {
        val user = sharedViewModel.user.value!!
        UserAPI.instance.signUp(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("User ${it.entityId} signed up successfully")

                    AuthCallExecutor.signIn(
                        user.email!!,
                        user.password!!,
                        object : ICallback<Boolean> {
                            override fun call(o: Boolean) {
                                if (o) {
                                    F2WLogger.info("User ${it.entityId} signed in successfully")
                                    ParentMainActivity.start(this@SignUpActivity, true)
                                } else {
                                    F2WLogger.warn("User ${it.entityId} failed to sign in")
                                    finish()
                                }
                            }
                        }
                    )
                },
                {
                    F2WLogger.error(it.message)

                    Snackbar.make(rootView, getString(R.string.no_internet_error_message), Snackbar.LENGTH_SHORT)
                        .setAction(getString(R.string.retry_label)) { handleSubmit() }
                        .show()
                }
            )
    }

    companion object {

        fun start(baseActivity: Activity, requestCode: Int? = null) {
            val intent = Intent(baseActivity, SignUpActivity::class.java)

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }

    private fun String?.isValidEmail(): Boolean =
        !this.isNullOrBlank() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    private fun String?.isValidPassword(): Boolean = !this.isNullOrBlank() && this.length >= 8
}

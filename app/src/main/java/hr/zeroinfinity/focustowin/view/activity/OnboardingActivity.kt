package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.IS_CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.view.adapter.OnboardingPagerAdapter
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnboardingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        val isChild = intent.getBooleanExtra(IS_CHILD_INTENT_CODE, false)
        tabDots.setupWithViewPager(mViewPager, true)
        mViewPager.adapter = OnboardingPagerAdapter(supportFragmentManager, isChild)

        joinButton.setOnClickListener {
            if (isChild) {
                QRPreviewActivity.start(this, isChild)
            } else {
                SignInActivity.start(this, false)
            }
        }
    }

    companion object {
        fun start(baseActivity: Activity, isChild: Boolean?, requestCode: Int? = null) {
            val intent = Intent(baseActivity, OnboardingActivity::class.java)

            intent.putExtra(IS_CHILD_INTENT_CODE, isChild)

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }
}

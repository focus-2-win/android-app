package hr.zeroinfinity.focustowin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.INDEX_INTENT_CODE
import hr.zeroinfinity.focustowin.config.IS_CHILD_INTENT_CODE
import kotlinx.android.synthetic.main.fragment_onboarding.*

class OnboardingFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_onboarding, null)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val index = arguments?.getInt(INDEX_INTENT_CODE, 0)
        val isChild = arguments?.getBoolean(IS_CHILD_INTENT_CODE, false)

        if (isChild != null && isChild) {
            when (index) {
                0 -> updateUI(
                    R.drawable.addiction,
                    R.string.onboarding_title_1_child,
                    R.string.onboarding_message_1_child
                )
                1 -> updateUI(R.drawable.reward, R.string.onboarding_title_2_child, R.string.onboarding_message_2_child)
                2 -> updateUI(
                    R.drawable.competition,
                    R.string.onboarding_title_3_child,
                    R.string.onboarding_message_3_child
                )
                else -> assert(false)
            }
        } else {
            when (index) {
                0 -> updateUI(R.drawable.addiction, R.string.onboarding_title_1, R.string.onboarding_message_1)
                1 -> updateUI(R.drawable.reward, R.string.onboarding_title_2, R.string.onboarding_message_2)
                2 -> updateUI(R.drawable.chores, R.string.onboarding_title_3, R.string.onboarding_message_3)
                3 -> updateUI(R.drawable.competition, R.string.onboarding_title_4, R.string.onboarding_message_4)
                else -> assert(false)
            }
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun updateUI(imageId: Int, titleId: Int, messageId: Int) {
        storyImageView.setImageResource(imageId)
        titleTextView.setText(titleId)
        messageTextView.setText(messageId)
    }

    companion object {

        @JvmStatic
        fun newInstance(index: Int, isChild: Boolean) =
            OnboardingFragment().apply {
                arguments = Bundle().apply {
                    putInt(INDEX_INTENT_CODE, index)
                    putBoolean(IS_CHILD_INTENT_CODE, isChild)
                }
            }
    }
}
package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.ICallback
import hr.zeroinfinity.focustowin.config.auth.AuthCallExecutor
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.onSubmit
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : BaseActivity() {

    private val shakeAnimation: Animation by lazy {
        AnimationUtils.loadAnimation(this@SignInActivity, R.anim.shake)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        signInButton.setOnClickListener { handleSignIn() }
        signUpButton.setOnClickListener { redirectToSignUp() }
        resetPasswordButton.setOnClickListener { handleResetPassword() }
        passwordInput.onSubmit { if (!emailInput.text.isNullOrBlank() && !passwordInput.text.isNullOrBlank()) handleSignIn() }
        setHideSoftKeyboardOnTouch(rootView, this)
    }

    private fun handleSignIn() {
        if (!isValidInput()) return

        val email = emailInput.text
        val password = passwordInput.text

        loadingOverlay.show()
        AuthCallExecutor.signIn(
            email.toString(),
            password.toString(),
            object : ICallback<Boolean> {
                override fun call(o: Boolean) {
                    runOnUiThread {
                        if (!o) {
                            loadingOverlay.dismiss()
                            passwordInput.text.clear()
                            emailInput.text.clear()

                            emailInput.error = getString(R.string.error_invalid_credentials)
                            emailInput.animation = shakeAnimation

                            emailInput.requestFocus()
                        } else {
                            F2WLogger.info("User $email logged in.")

                            ParentMainActivity.start(this@SignInActivity)
                        }
                    }
                }
            })
    }

    private fun isValidInput(): Boolean {
        val email = emailInput.text
        val password = passwordInput.text

        var r = true

        if (email.isNullOrBlank()) {
            email?.clear()
            emailInput.error = getString(R.string.error_required_field)
            emailInput.animation = shakeAnimation
            r = false
        }

        if (password.isNullOrBlank()) {
            password?.clear()
            passwordInput.error = getString(R.string.error_required_field)
            passwordInput.animation = shakeAnimation
            r = false
        }

        return r
    }

    private fun redirectToSignUp() {
        SignUpActivity.start(this)
    }

    private fun handleResetPassword() {
        //TODO: create popup or new screen with email input
    }

    companion object {

        fun start(baseActivity: Activity, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseActivity, SignInActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }
}

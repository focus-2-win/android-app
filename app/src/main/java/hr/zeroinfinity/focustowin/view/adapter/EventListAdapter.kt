package hr.zeroinfinity.focustowin.view.adapter

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.ChoreStatus
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.EventStatus
import hr.zeroinfinity.focustowin.view.custom.F2WEventReviewDialog
import hr.zeroinfinity.focustowin.view.custom.OnEventListener
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class EventListAdapter(
    private val context: Context,
    private val onEventUpdated: OnEventListener,
    private val isChild: Boolean = false
) :
    PagedListAdapter<Event, EventListAdapter.EventViewHolder>(Event.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        return EventViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.event_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val timeSpanTextView = itemView.findViewById<TextView>(R.id.timeSpanTextView)
        private val dateTextView = itemView.findViewById<TextView>(R.id.dateTextView)
        private val statusImageView = itemView.findViewById<ImageView>(R.id.statusImageView)
        private val choreCompletedImageView = itemView.findViewById<ImageView>(R.id.choreCompletedImageView)
        private val container = itemView.findViewById<ConstraintLayout>(R.id.container)

        fun bindTo(event: Event?) {
            if (event == null) return
            container.visibility = View.VISIBLE

            timeSpanTextView.text = itemView.context.getString(
                R.string.from_to_label_pattern, timeFormat.format(event.startTime), timeFormat.format(event.endTime)
            )
            dateTextView.text =
                itemView.context.getString(R.string.one_word_label_format, dateFormat.format(event.startTime))

            if (event.endTime?.before(Date())!!) {
                if (event.status == EventStatus.SUCCEEDED) {
                    statusImageView.setImageResource(R.drawable.ic_status_succeeded)
                } else {
                    statusImageView.setImageResource(R.drawable.ic_status_failed)
                }

                itemView.setOnClickListener {
                    if (isChild) {
                        F2WEventReviewDialog(context, {}, event, true).show()
                    } else {
                        F2WEventReviewDialog(context, onEventUpdated, event).show()
                    }
                }
            } else {
                statusImageView.setImageResource(when (event.status) {
                    EventStatus.SUCCEEDED -> {
                        itemView.setOnClickListener {
                            if (isChild) {
                                F2WEventReviewDialog(context, {}, event, true).show()
                            } else {
                                F2WEventReviewDialog(context, onEventUpdated, event).show()
                            }
                        }

                        R.drawable.ic_status_succeeded
                    }
                    EventStatus.PENDING -> {
                        itemView.setOnClickListener {
                            if (isChild) {
                                onEventUpdated.invoke(event)
                            }
                        }
                        R.drawable.ic_status_pending
                    }
                    else -> {
                        itemView.setOnClickListener {
                            if (isChild) {
                                F2WEventReviewDialog(context, {}, event, true).show()
                            } else {
                                F2WEventReviewDialog(context, onEventUpdated, event).show()
                            }
                        }

                        R.drawable.ic_status_failed
                    }
                })
            }

            if (event.chores.isNullOrEmpty() || event.chores?.any { it.status == ChoreStatus.UNCOMPLETED }!!) {
                choreCompletedImageView.visibility = View.INVISIBLE
            } else {
                choreCompletedImageView.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        private val timeFormat: DateFormat = SimpleDateFormat("HH:mm", Locale.US)
        private val dateFormat: DateFormat = SimpleDateFormat("MMM, d", Locale.US)
    }
}
package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.SeekBar
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.Wish
import kotlinx.android.synthetic.main.activity_new_event.*
import kotlinx.android.synthetic.main.dialog_custom.*

typealias ApproveDialogListener = (Wish) -> Unit

class F2WApproveWishDialog(
    context: Context,
    private val listener: ApproveDialogListener,
    private val wish: Wish

) : Dialog(context) {

    private val minMonths = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_approve_wish)

        durationSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (progress < minMonths) {
                    seekBar?.progress = minMonths

                    updateDuration(minMonths)
                } else {
                    updateDuration(progress)
                }
            }
        })

        positiveButton.setOnClickListener {
            dismiss()
            wish.targetPoints = durationSeekBar.progress * 20_000
            listener.invoke(wish)
        }

        negativeButton.setOnClickListener {
            dismiss()
        }

        updateDuration(minMonths)
        durationSeekBar.progress = minMonths
    }

    private fun updateDuration(months: Int) {
        durationTextView.text = context.getString(R.string.months_format, months)
    }
}
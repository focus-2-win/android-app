package hr.zeroinfinity.focustowin.view.fragment

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.config.PARENT_INTENT_CODE
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.User
import hr.zeroinfinity.focustowin.view.activity.ParentMainActivity
import hr.zeroinfinity.focustowin.view.activity.QRScanActivity
import hr.zeroinfinity.focustowin.view.adapter.ChildrenGridAdapter
import hr.zeroinfinity.focustowin.view.adapter.OnChildSelectedListener
import hr.zeroinfinity.focustowin.view.adapter.ParentsGridAdapter
import hr.zeroinfinity.focustowin.view.custom.F2WDialog
import hr.zeroinfinity.focustowin.view.viewmodel.ParentMainViewModel
import kotlinx.android.synthetic.main.fragment_family.*

class FamilyFragment : Fragment() {

    private lateinit var sharedViewModel: ParentMainViewModel

    private val onRemoveChildListener: OnChildSelectedListener = {
        val family = sharedViewModel.family.value

        family?.minors?.remove(it)
        (gridViewChildren.adapter as ArrayAdapter<*>).notifyDataSetChanged()
        sharedViewModel.family.postValue(family)
    }

    private val onChildSelectedListener: OnChildSelectedListener = {
        (activity as? ParentMainActivity)?.invoke(it)
    }

    private val addNewMemberListener: View.OnClickListener = View.OnClickListener {
        val dialog = F2WDialog.builder(context!!)
            .setTitle(getString(R.string.add_new_member_label))
            .setMessage(getString(R.string.add_new_member_question))
            .setPositiveButtonText(getString(R.string.child_label))
            .setNegativeButtonText(getString(R.string.parent_label))
            .setPositiveButtonListener(View.OnClickListener {
                QRScanActivity.start(
                    this,
                    true,
                    sharedViewModel.family.value?.id!!,
                    ADD_CHILD_REQ_CODE
                )
            })
            .setNegativeButtonListener(View.OnClickListener {
                QRScanActivity.start(
                    this,
                    false,
                    sharedViewModel.family.value?.id!!,
                    ADD_PARENT_REQ_CODE
                )
            })
            .setCanceledOnTouchOutside(true)
            .bothPositive(true)
            .build()

        dialog.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(it).get(ParentMainViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_family, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        gridViewParents.adapter = ParentsGridAdapter(context!!, sharedViewModel.family.value?.parents!!.toMutableList())
        gridViewChildren.adapter = ChildrenGridAdapter(
            context!!,
            sharedViewModel.family.value?.minors!!.toMutableList(),
            onRemoveChildListener,
            onChildSelectedListener
        )

        if (sharedViewModel.family.value?.minors!!.size == 0) {
            gridViewChildren.visibility = View.INVISIBLE
            gridViewChildrenPlaceholder.visibility = View.VISIBLE

        } else {
            gridViewChildren.visibility = View.VISIBLE
            gridViewChildrenPlaceholder.visibility = View.GONE

        }

        familyNameTextView.text = getString(
            R.string.family_name_label,
            sharedViewModel.family.value?.parents!!.find { it.isFamilyAdmin!! }?.surname
        )
        addNewMemberButton.setOnClickListener(addNewMemberListener)

        sharedViewModel.family.observe(this, Observer {
            (gridViewParents.adapter as ParentsGridAdapter).refresh(it?.parents!!)
            (gridViewChildren.adapter as ChildrenGridAdapter).refresh(it.minors!!)

            if (it.minors!!.size == 0) {
                gridViewChildren.visibility = View.INVISIBLE
                gridViewChildrenPlaceholder.visibility = View.VISIBLE
            } else {
                gridViewChildren.visibility = View.VISIBLE
                gridViewChildrenPlaceholder.visibility = View.GONE
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val family = sharedViewModel.family.value!!
            when (requestCode) {
                ADD_CHILD_REQ_CODE -> {
                    val child = data?.getParcelableExtra<Child>(CHILD_INTENT_CODE)
                    family.minors?.add(child!!)

                    sharedViewModel.family.postValue(family)
                }
                ADD_PARENT_REQ_CODE -> {
                    val parent = data?.getParcelableExtra<User>(PARENT_INTENT_CODE)
                    family.parents?.add(parent!!)

                    sharedViewModel.family.postValue(family)
                }
            }
        }
    }
    companion object {
        const val ADD_CHILD_REQ_CODE = 111
        const val ADD_PARENT_REQ_CODE = 222


        @JvmStatic
        fun newInstance() = FamilyFragment()
    }
}

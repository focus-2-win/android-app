package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.EventStatus
import hr.zeroinfinity.focustowin.view.adapter.ChoirReviewListAdapter
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.dialog_chores_review.*
import kotlinx.android.synthetic.main.dialog_chores_review.choresListView

typealias OnEventListener = (Event) -> Unit

class F2WEventReviewDialog(
    context: Context,
    private val onEventUpdated: OnEventListener,
    private val event: Event,
    private val isChild: Boolean = false
) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation
        setCanceledOnTouchOutside(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.dialog_chores_review)

        statusImageView.setImageResource(
            when (event.status) {
                EventStatus.SUCCEEDED -> R.drawable.ic_status_succeeded
                else -> R.drawable.ic_status_failed
            }
        )

        statusTextView.setText(
            when (event.status) {
                EventStatus.SUCCEEDED -> R.string.succeeded_label
                else -> R.string.failed_label
            }
        )

        if (isChild) {
            messageTextView.text = context.getString(R.string.list_of_chores_label)
            positiveButton.setText(R.string.ok_label)

            if (event.status == EventStatus.SUCCEEDED) {
                pointsEarnedTextView.visibility = View.VISIBLE
                pointsEarnedTextView.text = context.getString(
                    R.string.points_earned_format,
                    event.points.toString() //TODO check
                )
            }

            negativeButton.visibility = View.GONE
        }

        when {
            event.chores == null -> {
                messageTextView.visibility = View.GONE
                choresListView.visibility = View.GONE
                positiveButton.setText(R.string.ok_label)

                positiveButton.setOnClickListener {
                    dismiss()
                }
            }
            event.chores?.size == 0 -> {
                choreContainer.visibility = View.GONE
                positiveButton.setOnClickListener {
                    dismiss()
                }
            }
            else -> {
                choresListView.adapter = ChoirReviewListAdapter(context, event.chores!!, isChild)

                positiveButton.setOnClickListener {
                    onEventUpdated.invoke(event)
                    dismiss()
                }
            }
        }

        negativeButton.setOnClickListener {
            dismiss()
        }
    }

    override fun onBackPressed() {
        dismiss()
    }
}
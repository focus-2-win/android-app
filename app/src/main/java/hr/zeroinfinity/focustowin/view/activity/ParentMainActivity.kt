package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.view.Window
import android.view.WindowManager
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.FamilyAPI
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.view.adapter.OnChildSelectedListener
import hr.zeroinfinity.focustowin.view.custom.F2WDialog
import hr.zeroinfinity.focustowin.view.fragment.ApplicationInAlphaFragment
import hr.zeroinfinity.focustowin.view.fragment.ChildrenFragment
import hr.zeroinfinity.focustowin.view.fragment.FamilyFragment
import hr.zeroinfinity.focustowin.view.viewmodel.ParentMainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_parent_main.*
import retrofit2.HttpException

class ParentMainActivity : AuthBaseActivity(false), OnChildSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private lateinit var sharedViewModel: ParentMainViewModel
    private var currentFragmentIndex: Int = 0
    private var currentNavItemId: Int = R.id.navigation_family
    private var selectedChild: Child? = null

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        if (currentNavItemId == item.itemId) return@OnNavigationItemSelectedListener false

        if (sharedViewModel.family.value?.minors?.size!! == 0) {
            F2WDialog.builder(this@ParentMainActivity)
                .setTitle(getString(R.string.warning_label))
                .setMessage(getString(R.string.no_child_in_family_message))
                .setPositiveButtonText(getString(R.string.i_understand_label))
                .build()
                .show()

            return@OnNavigationItemSelectedListener false
        }

        currentNavItemId = item.itemId
        var fragment: Fragment? = null

        val previousFragmentIndex = currentFragmentIndex
        when (item.itemId) {
            R.id.navigation_family -> {
                fragment = FamilyFragment.newInstance()
                currentFragmentIndex = 0

            }
            R.id.navigation_kids -> {
                fragment = ChildrenFragment.newInstance(selectedChild)
                selectedChild = null
                currentFragmentIndex = 1
            }
            R.id.navigation_statistics -> {
                fragment = ApplicationInAlphaFragment.newInstance()
                currentFragmentIndex = 2
            }
        }

        loadFragment(fragment, currentFragmentIndex > previousFragmentIndex)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_parent_main)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        rootView.setOnRefreshListener(this)

        loadingOverlay.show()

        logoutButton.setOnClickListener {
            AuthContextHolder.instance.removeToken()
            SignInActivity.start(this, true)
        }

        fetchFamily(true)
    }

    override fun invoke(p1: Child) {
        selectedChild = p1

        navView.selectedItemId = R.id.navigation_kids
    }

    override fun onRefresh() {
        fetchFamily(false)
        rootView.isRefreshing = false
    }

    private fun loadFragment(fragment: Fragment?, slideLeft: Boolean): Boolean {
        var anim1 = R.anim.slide_in_left
        var anim2 = R.anim.slide_out_right
        if (slideLeft) {
            anim1 = R.anim.slide_in_right
            anim2 = R.anim.slide_out_left
        }

        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(anim1, anim2)
                .replace(R.id.fragmentContainer, fragment)
                .commit()
            return true
        }
        return false
    }

    private fun fetchFamily(isInitial: Boolean) {
        FamilyAPI.instance.getFamily()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("Family ${it.id} fetched.")

                    sharedViewModel =
                        ViewModelProviders.of(this@ParentMainActivity).get(ParentMainViewModel::class.java)
                    sharedViewModel.family.postValue(it)

                    if (isInitial) {
                        loadingOverlay.dismiss()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.fragmentContainer, FamilyFragment.newInstance())
                            .commit()
                    }


                },
                {
                    F2WLogger.info("Family failed to fetch")
                    loadingOverlay.dismiss()

                    if (it is HttpException && it.code() == 404) {
                        F2WLogger.info("User ${AuthContextHolder.instance.getUserData()?.get("id")} doesn't have a family.")

                        PostSignUpActivity.start(this@ParentMainActivity, true)
                    } else {
                        F2WLogger.error(it.message)
                        Snackbar.make(
                            rootView,
                            getString(R.string.no_internet_error_message),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.retry_label) {
                                fetchFamily(isInitial)
                            }
                            .show()
                    }
                }
            )
    }

    companion object {

        fun start(baseActivity: Activity, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseActivity, ParentMainActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }
}

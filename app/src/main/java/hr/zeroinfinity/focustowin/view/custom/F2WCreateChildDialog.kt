package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.internal.Avatar
import hr.zeroinfinity.focustowin.view.adapter.AvatarSelectorAdapter
import hr.zeroinfinity.focustowin.view.adapter.TextWatcherAdapter
import kotlinx.android.synthetic.main.dialog_create_child.*
import kotlinx.android.synthetic.main.dialog_custom.negativeButton
import kotlinx.android.synthetic.main.dialog_custom.positiveButton
import kotlinx.android.synthetic.main.dialog_with_input_custom.input

typealias CreateChildDialogListener = (String, Int) -> Unit
typealias CreateChildDialogDismissListener = (Unit) -> Unit

class F2WCreateChildDialog private constructor(
    context: Context,
    private val listener: CreateChildDialogListener?,
    private val createChildDialogDismissListener: CreateChildDialogDismissListener?,
    private val child: Child?

) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_create_child)
        setCanceledOnTouchOutside(false)

        positiveButton.setOnClickListener {
            dismiss()
            listener?.invoke(
                input.text.toString(),
                (avatarsRecyclerView.adapter as AvatarSelectorAdapter).selectedAvatarId
            )
        }

        negativeButton.setOnClickListener {
            dismiss()
            createChildDialogDismissListener?.invoke(Unit)
        }

        val adapter = AvatarSelectorAdapter()
        if (child != null) {
            adapter.refresh(Avatar.ALL.first { it.id == child.avatarId })
            input.setText(child.username)
            positiveButton.isEnabled = true
        } else {
            positiveButton.isEnabled = false
        }

        avatarsRecyclerView.adapter = adapter
        input.addTextChangedListener(object : TextWatcherAdapter() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                positiveButton.isEnabled = !s.isNullOrEmpty()
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        createChildDialogDismissListener?.invoke(Unit)
    }

    class Builder(private val context: Context) {
        private var listener: CreateChildDialogListener? = null
        private var createChildDialogDismissListener: CreateChildDialogDismissListener? = null
        private var child: Child? = null

        fun setListener(_listener: CreateChildDialogListener): Builder {
            listener = _listener
            return this
        }

        fun setChild(_child: Child): Builder {
            child = _child
            return this
        }

        fun setDismissListener(_CreateChildDialog_dismissListener: CreateChildDialogDismissListener): Builder {
            createChildDialogDismissListener = _CreateChildDialog_dismissListener
            return this
        }

        fun build(): F2WCreateChildDialog {
            return F2WCreateChildDialog(
                context,
                listener,
                createChildDialogDismissListener,
                child
            )
        }
    }

    companion object {
        fun builder(context: Context): Builder {
            return Builder(context)
        }
    }
}
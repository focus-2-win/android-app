package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import hr.zeroinfinity.focustowin.R
import kotlinx.android.synthetic.main.dialog_custom.messageTextView
import kotlinx.android.synthetic.main.dialog_custom.negativeButton
import kotlinx.android.synthetic.main.dialog_custom.positiveButton
import kotlinx.android.synthetic.main.dialog_custom.titleTextView
import kotlinx.android.synthetic.main.dialog_with_input_custom.*

typealias DialogWithInputListener = (String, Boolean) -> Unit

class F2WDialogWithInput private constructor(
    context: Context,
    private val titleText: String,
    private var messageText: String,
    private val positiveBtnText: String,
    private val negativeBtnText: String?,
    private val listener: DialogWithInputListener,
    private val canceledOnTouch: Boolean

) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_with_input_custom)
        setCanceledOnTouchOutside(canceledOnTouch)

        titleTextView.text = titleText
        messageTextView.text = messageText

        positiveButton.text = positiveBtnText
        positiveButton.setOnClickListener {
            dismiss()
            if (input.text.isNullOrEmpty()) {
                input.error = context.getString(R.string.error_required_field)
            } else {
                listener.invoke(input.text.toString(), true)
            }
        }

        if (negativeBtnText == null) {
            negativeButton.visibility = View.GONE
        } else {
            negativeButton.text = negativeBtnText
            negativeButton.setOnClickListener {
                dismiss()
                listener.invoke(input.text.toString(), false)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        listener.invoke(input.text.toString(), false)
    }

    class Builder(private val context: Context) {
        private var title: String = ""
        private var message: String = ""
        private var positiveBtnText: String = "OK"
        private var negativeBtnText: String? = null
        private var listener: DialogWithInputListener? = null
        private var cot: Boolean = true

        fun setTitle(_title: String): Builder {
            title = _title
            return this
        }

        fun setMessage(_message: String): Builder {
            message = _message
            return this
        }

        fun setPositiveButtonText(_text: String): Builder {
            positiveBtnText = _text
            return this
        }

        fun setNegativeButtonText(_text: String): Builder {
            negativeBtnText = _text
            return this
        }

        fun setListener(_listener: DialogWithInputListener): Builder {
            listener = _listener
            return this
        }

        fun setCanceledOnTouchOutside(_cot: Boolean): Builder {
            cot = _cot
            return this
        }


        fun build(): F2WDialogWithInput {
            return F2WDialogWithInput(
                context,
                title,
                message,
                positiveBtnText,
                negativeBtnText,
                listener!!,
                cot
            )
        }
    }

    companion object {
        fun builder(context: Context): Builder {
            return Builder(context)
        }
    }
}
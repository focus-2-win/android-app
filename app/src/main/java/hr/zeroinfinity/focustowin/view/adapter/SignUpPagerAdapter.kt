package hr.zeroinfinity.focustowin.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import hr.zeroinfinity.focustowin.view.fragment.SignUpFragment

class SignUpPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {

    override fun getItem(index: Int): Fragment {
        return SignUpFragment.newInstance(index)
    }

    override fun getCount(): Int = 4
}
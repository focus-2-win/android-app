package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import hr.zeroinfinity.focustowin.config.event.*
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.view.custom.F2WLoadingOverlay

abstract class BaseActivity : AppCompatActivity(), EventBus.EventListener {

    protected val loadingOverlay: F2WLoadingOverlay by lazy {
        F2WLoadingOverlay(this)
    }

    override fun onStart() {
        super.onStart()

        EventBus.register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.unregister(this)
    }


    override fun update(data: Event) {
        when (data) {
            is UnauthorizedEvent -> F2WLogger.info("Unauthorized event")
            is ConnectionLostEvent -> F2WLogger.info("Connection lost")
            is ConnectionEstablishedEvent -> F2WLogger.info("Connection established")
        }
    }

    companion object {
        fun setHideSoftKeyboardOnTouch(view: View, activity: Activity) {
            if (view !is EditText) {
                view.setOnTouchListener { _, _ ->
                    hideSoftKeyboard(activity)
                    false
                }
            }

            if (view is ViewGroup) {
                for (i in 0 until view.childCount) {
                    val innerView = view.getChildAt(i)
                    setHideSoftKeyboardOnTouch(innerView, activity)
                }
            }
        }

        private fun hideSoftKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus

            if (view == null) {
                view = View(activity)
            }

            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}
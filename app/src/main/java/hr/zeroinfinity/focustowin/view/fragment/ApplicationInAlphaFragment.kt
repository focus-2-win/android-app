package hr.zeroinfinity.focustowin.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.view.viewmodel.ParentMainViewModel

class ApplicationInAlphaFragment : Fragment() {
    private lateinit var sharedViewModel: ParentMainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(it).get(ParentMainViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_application_in_alpha, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance() = ApplicationInAlphaFragment()
    }
}
package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.widget.DatePicker
import android.widget.SeekBar
import android.widget.Toast
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.CHILD_ID_INTENT_CODE
import hr.zeroinfinity.focustowin.config.WISHES_LIST_INTENT_CODE
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.EventAPI
import hr.zeroinfinity.focustowin.model.Chore
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.Wish
import hr.zeroinfinity.focustowin.view.adapter.ChoreListAdapter
import hr.zeroinfinity.focustowin.view.adapter.WishListAdapter
import hr.zeroinfinity.focustowin.view.custom.F2WCreateChoirDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_new_event.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class NewEventActivity : AuthBaseActivity(false), DatePickerDialog.OnDateSetListener {

    private var eventDate: Date = Date()
    private var hourFrom: Int = 12
    private var minuteFrom: Int = 0
    private var hourTo: Int = 18
    private var minuteTo: Int = 0
    private var childId: Long = -1
    private var wish: Wish? = null
    private lateinit var wishes: MutableList<Wish>
    private var chores: MutableList<Chore> = mutableListOf()

    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var fromTimePickerDialog: TimePickerDialog
    private lateinit var toTimePickerDialog: TimePickerDialog

    private val dateFormat: DateFormat = SimpleDateFormat("yyyy-MMM-dd", Locale.US)
    private val timeFormat: String = "%02d:%02d"
    private val minMinutes = 30

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_event)

        intent.let {
            childId = it.getLongExtra(CHILD_ID_INTENT_CODE, -1)
            wishes = it.getParcelableArrayListExtra<Wish>(WISHES_LIST_INTENT_CODE).toMutableList()
        }

        val cal = Calendar.getInstance()
        cal.time = eventDate
        val currentYear = cal.get(Calendar.YEAR)
        val currentMonth = cal.get(Calendar.MONTH)
        val currentDay = cal.get(Calendar.DAY_OF_MONTH)

        datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
        datePickerDialog.datePicker.minDate = eventDate.time
        datePickerButton.setOnClickListener {
            datePickerDialog.show()
        }
        datePickerButton.text = dateFormat.format(eventDate)

        fromTimePickerDialog = TimePickerDialog(this,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                if (hourOfDay < 7 || hourOfDay > 21) {
                    Toast.makeText(this@NewEventActivity, R.string.from_time_warning_message, Toast.LENGTH_SHORT).show()
                    hourFrom = 7
                    minuteFrom = 0
                } else {
                    hourFrom = hourOfDay
                    minuteFrom = minute
                }

                refreshFromTime()
                correctTime()
            }, hourFrom, minuteFrom, true
        )
        fromTimePickerButton.setOnClickListener {
            fromTimePickerDialog.show()
        }
        refreshFromTime()

        toTimePickerDialog = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            if (hourOfDay < 8 || hourOfDay > 22) {
                Toast.makeText(this, getString(R.string.to_time_warning_message), Toast.LENGTH_SHORT).show()
                hourTo = hourFrom + 1
                minuteTo = minuteFrom
            } else {
                hourTo = hourOfDay
                minuteTo = minute
            }

            refreshToTime()
            correctTime()
        }, hourTo, minuteTo, true)
        toTimePickerButton.setOnClickListener {
            toTimePickerDialog.show()
        }
        refreshToTime()

        durationSeekBar.max = calculateMinutes()
        durationSeekBar.progress = minMinutes
        updateDuration(minMinutes)
        durationSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (progress < minMinutes) {
                    seekBar?.progress = minMinutes

                    updateDuration(minMinutes)
                } else {
                    updateDuration(progress)
                }
            }
        })

        addChoreButton.setOnClickListener {
            F2WCreateChoirDialog(this@NewEventActivity, {
                chores.add(it)
                choresRecycleView.adapter?.notifyDataSetChanged()
            }).show()
        }

        choresRecycleView.adapter = ChoreListAdapter(chores, {
            chores.remove(it)
            choresRecycleView.adapter?.notifyDataSetChanged()
        }, {
            for (chore in chores) {
                if (chore.id != it.id) continue

                chore.points = it.points
                chore.name = it.name

                break
            }

            choresRecycleView.adapter?.notifyDataSetChanged()
        })

        wish = wishes[0]
        wishListRecyclerView.adapter = WishListAdapter(wishes, { w -> wish = w }, null, wish)

        cancelButton.setOnClickListener {
            finish()
        }

        createButton.setOnClickListener {
            val cal = Calendar.getInstance()
            cal.time = eventDate
            val currentYear = cal.get(Calendar.YEAR)
            val currentMonth = cal.get(Calendar.MONTH)
            val currentDay = cal.get(Calendar.DAY_OF_MONTH)

            val event = Event(
                null,
                wish?.id!!,
                GregorianCalendar(currentYear, currentMonth, currentDay, hourFrom, minuteFrom).time,
                GregorianCalendar(currentYear, currentMonth, currentDay, hourTo, minuteTo).time,
                durationSeekBar.progress,
                null,
                null,
                null,
                chores
            )

            createEvent(event)
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        eventDate = GregorianCalendar(year, month, dayOfMonth).time
        datePickerButton.text = dateFormat.format(eventDate)
    }

    private fun correctTime() {
        if (calculateMinutes() < 60) {
            Toast.makeText(this, getString(R.string.interval_warning_message), Toast.LENGTH_SHORT).show()
            hourTo = hourFrom + 1
            minuteTo = minuteFrom

            refreshToTime()
        }

        updateMinutesMax()
    }

    private fun refreshFromTime() {
        fromTimePickerDialog.updateTime(hourFrom, minuteFrom)
        fromTimePickerButton.text = String.format(timeFormat, hourFrom, minuteFrom)
    }

    private fun refreshToTime() {
        toTimePickerDialog.updateTime(hourTo, minuteTo)
        toTimePickerButton.text = String.format(timeFormat, hourTo, minuteTo)
    }

    private fun updateMinutesMax() {
        val minutes = calculateMinutes()
        durationSeekBar.progress = if (minutes < durationSeekBar.progress) minutes else durationSeekBar.progress
        durationSeekBar.max = minutes
    }

    private fun updateDuration(minutes: Int) {
        durationTextView.text = getString(R.string.minutes_format, minutes)
    }

    private fun calculateMinutes(): Int {
        return (hourTo - hourFrom) * 60 + minuteFrom - minuteTo
    }

    private fun createEvent(event: Event) {
        loadingOverlay.show()
        EventAPI.instance.create(event)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    loadingOverlay.dismiss()
                    F2WLogger.info("Event ${it.entityId} created.")

                    val intent = Intent()
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                },
                {
                    loadingOverlay.dismiss()
                    F2WLogger.error(it.message)

                    Snackbar.make(
                        rootView,
                        getString(R.string.no_internet_error_message),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(R.string.retry_label) { createEvent(event) }
                        .show()
                }
            )
    }
    companion object {
        fun start(
            baseFragment: Fragment,
            asNewTask: Boolean = true,
            requestCode: Int? = null,
            childId: Long,
            wishes: List<Wish>
        ) {
            val intent = Intent(baseFragment.context, NewEventActivity::class.java)
            intent.putExtra(CHILD_ID_INTENT_CODE, childId)
            intent.putParcelableArrayListExtra(WISHES_LIST_INTENT_CODE, ArrayList(wishes))

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseFragment.startActivity(intent)
            } else {
                baseFragment.startActivityForResult(intent, requestCode)
            }
        }
    }
}

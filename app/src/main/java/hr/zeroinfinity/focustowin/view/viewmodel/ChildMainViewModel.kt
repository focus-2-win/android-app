package hr.zeroinfinity.focustowin.view.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.Family

class ChildMainViewModel : ViewModel() {

    val family = MutableLiveData<Family>()

    fun getActiveChild(): Child? {
        return family.value?.minors?.firstOrNull { child -> child.id == AuthContextHolder.instance.getChildId() }
    }
}
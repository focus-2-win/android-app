package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.CHILD_TOKEN_KEY_SHARED_PREFERENCES
import hr.zeroinfinity.focustowin.config.ID_INTENT_CODE
import hr.zeroinfinity.focustowin.config.IS_CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.ChildAPI
import hr.zeroinfinity.focustowin.config.retrofit.api.FamilyAPI
import hr.zeroinfinity.focustowin.view.custom.F2WDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity__qr_preview.*
import retrofit2.HttpException

class QRPreviewActivity : BaseActivity() {

    private var _isChild: Boolean = false
    private var _id: Long = Long.MIN_VALUE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity__qr_preview)

        _id = intent.getLongExtra(ID_INTENT_CODE, Long.MIN_VALUE)
        _isChild = intent.getBooleanExtra(IS_CHILD_INTENT_CODE, false)

        if (!_isChild) {
            initQR()
        } else {
            fetchId()
        }

        if (_isChild) {
            logoutButton.visibility = View.VISIBLE

            logoutButton.setOnClickListener {
                ChildAPI.instance.delete(_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            //TODO add dialog "are you sure"
                            F2WLogger.info("Child $_id deleted and logged out.")
                            AuthContextHolder.instance.removeToken()
                            PreSignInActivity.start(this, true)
                        },
                        {
                            F2WLogger.error(it.message)
                            Snackbar.make(
                                rootView,
                                getString(R.string.no_internet_error_message),
                                Snackbar.LENGTH_SHORT
                            )
                                .setAction(getString(R.string.dismiss_label)) { }
                                .show()
                        }
                    )
            }
        }

        backButton.setOnClickListener { finish() }
        joinButton.setOnClickListener { checkIfJoined() }
    }

    private fun checkIfJoined() {
        loadingOverlay.show()
        FamilyAPI.instance.getFamily(_id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    loadingOverlay.dismiss()
                    F2WLogger.info("Child $_id joined the family ${it.id}.")

                    ChildMainActivity.start(this, true)
                },
                {
                    loadingOverlay.dismiss()
                    F2WLogger.error(it.message)

                    if (it is HttpException && it.code() == 400) {
                        F2WDialog.builder(this)
                            .setTitle(getString(R.string.qr_alert_not_in_family_title))
                            .setMessage(getString(R.string.qr_alert_not_in_family_message))
                            .setPositiveButtonText(getString(R.string.dismiss_label))
                            .build()
                            .show()

                    } else {
                        Snackbar.make(rootView, getString(R.string.no_internet_error_message), Snackbar.LENGTH_SHORT)
                            .setAction(getString(R.string.retry_label)) { checkIfJoined() }
                            .show()
                    }
                }
            )
    }

    private fun fetchId() {
        _id = PreferenceManager.getDefaultSharedPreferences(this).getLong(CHILD_TOKEN_KEY_SHARED_PREFERENCES, -1L)
        if (_id == -1L) {
            loadingOverlay.show()
            ChildAPI.instance.create()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        loadingOverlay.dismiss()
                        _id = it.id!!
                        AuthContextHolder.instance.setToken(_id)

                        F2WLogger.info("Child $_id created.")
                        initQR()
                    },
                    {
                        loadingOverlay.dismiss()
                        F2WLogger.error(it.message)

                        Snackbar.make(
                            rootView,
                            getString(R.string.no_internet_error_message),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(R.string.retry_label) { fetchId() }
                            .show()
                    }
                )
        } else {
            initQR()
        }
    }

    private fun initQR() {
        try {
            val bitMatrix = multiFormatWriter.encode(
                _id.toString(), BarcodeFormat.QR_CODE, 300, 300
            )
            qrView.setImageBitmap(barcodeEncoder.createBitmap(bitMatrix))

        } catch (e: WriterException) {
            F2WLogger.error(e.message)
            qrView.visibility = View.GONE
        }

        codeTextView.text = getString(R.string.code_format, codeTextView.text.toString(), _id)
    }


    companion object {
        private val barcodeEncoder = BarcodeEncoder()
        private val multiFormatWriter = MultiFormatWriter()

        fun start(
            baseActivity: Activity,
            isChild: Boolean,
            id: Long? = null,
            requestCode: Int? = null,
            asNewTask: Boolean = false
        ) {
            val intent = Intent(baseActivity, QRPreviewActivity::class.java)

            intent.putExtra(ID_INTENT_CODE, id)
            intent.putExtra(IS_CHILD_INTENT_CODE, isChild)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }

        fun start(
            baseFragment: Fragment,
            isChild: Boolean,
            id: Long? = null,
            requestCode: Int? = null,
            asNewTask: Boolean = false
        ) {

            val intent = Intent(baseFragment.context, QRPreviewActivity::class.java)

            intent.putExtra(ID_INTENT_CODE, id)
            intent.putExtra(IS_CHILD_INTENT_CODE, isChild)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            if (requestCode == null) {
                baseFragment.startActivity(intent)
            } else {
                baseFragment.startActivityForResult(intent, requestCode)
            }
        }
    }
}

package hr.zeroinfinity.focustowin.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.Refreshable
import hr.zeroinfinity.focustowin.model.Chore
import hr.zeroinfinity.focustowin.view.custom.F2WCreateChoirDialog
import hr.zeroinfinity.focustowin.view.custom.OnChoirListener

typealias OnChoirRemoved = (Chore) -> Unit

class ChoreListAdapter(
    private var chores: MutableList<Chore>,
    private val onChoirRemoved: OnChoirRemoved,
    private val onUpdateChoir: OnChoirListener

) : RecyclerView.Adapter<ChoreListAdapter.ViewHolder>(),
    Refreshable<List<Chore>> {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.chore_list_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = chores.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(chores[position])
    }

    override fun refresh(newData: List<Chore>) {
        chores.clear()
        chores.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val description = view.findViewById<TextView>(R.id.choreTextView)
        private val difficulty = view.findViewById<ImageView>(R.id.difficultyImageView)
        private val removeButton = view.findViewById<ImageButton>(R.id.removeButton)

        fun bind(chore: Chore) {
            description.text = chore.name

            when {
                chore.points in 50..100 -> {
                    difficulty.setImageResource(R.drawable.ic_star_easy)
                }
                chore.points in 100..200 -> {
                    difficulty.setImageResource(R.drawable.ic_star_medium)
                }
                else -> {
                    difficulty.setImageResource(R.drawable.ic_star_hard)
                }
            }

            removeButton.setOnClickListener {
                onChoirRemoved.invoke(chore)
            }

            view.setOnClickListener {
                F2WCreateChoirDialog(view.context, onUpdateChoir, chore).show()
            }
        }
    }
}
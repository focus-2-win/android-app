package hr.zeroinfinity.focustowin.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.Refreshable
import hr.zeroinfinity.focustowin.model.Child

typealias OnChildSelectedListener = (Child) -> Unit

class ChildrenGridAdapter(
    context: Context,
    private val children: MutableList<Child>,
    private val onRemoveChildListener: OnChildSelectedListener,
    private val onChildSelectedListener: OnChildSelectedListener
) : ArrayAdapter<Child>(context, R.layout.child_grid_item, children), Refreshable<List<Child>> {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val child = getItem(position)

        var cv = convertView
        if (convertView == null) {
            cv = LayoutInflater.from(context).inflate(R.layout.child_grid_item, null)
        }

        cv?.findViewById<TextView>(R.id.textView)?.text = child?.username

        //TODO add ability to remove child on long click if user is family admin
        //cv?.setOnLongClickListener {
        //    val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        //    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        //        v.vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE))
        //    } else {
        //        v.vibrate(150)
        //    }
        //    F2WDialog.builder(context)
        //        .setTitle(context.getString(R.string.warning_label))
        //        .setMessage(context.getString(R.string.delete_child_message, child.username))
        //        .setPositiveButtonText(context.getString(R.string.yes_label))
        //        .setNegativeButtonText(context.getString(R.string.no_label))
        //        .setPositiveButtonListener(View.OnClickListener {
        //           //TODO API call
        //            onRemoveChildListener(child)
        //        })
        //        .build()
        //        .show()
        //    false
        //}

        cv?.setOnClickListener {
            onChildSelectedListener(child)
        }

        cv?.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> v.alpha = 0.75f
                MotionEvent.ACTION_UP -> v.alpha = 1f
            }
            false
        }

        return cv!!
    }

    override fun refresh(newData: List<Child>) {
        children.clear()
        children.addAll(newData)
        notifyDataSetChanged()
    }
}
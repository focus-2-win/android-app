package hr.zeroinfinity.focustowin.view.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat.getColor
import android.view.MotionEvent
import android.view.View
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.ICallback
import hr.zeroinfinity.focustowin.config.AppMode
import hr.zeroinfinity.focustowin.config.auth.AuthCallExecutor
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.view.util.CustomAnimator
import kotlinx.android.synthetic.main.activity_pre_sign_in.*

class PreSignInActivity : BaseActivity() {

    private var repeatedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            window.statusBarColor = getColor(this, R.color.transparentFull)
        }

        setContentView(R.layout.activity_pre_sign_in)

        loadingAnimationView.speed = 4f
        loadingAnimationView.playAnimation()
        loadingAnimationView.addAnimatorListener(
            object : AnimatorListenerAdapter() {
                override fun onAnimationRepeat(animation: Animator?) {
                    super.onAnimationRepeat(animation)

                    if (!repeatedOnce) {
                        repeatedOnce = true
                        initAuth()
                    }
                }
            })

        parentModeButton.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> v.alpha = 0.75f
                MotionEvent.ACTION_UP -> v.alpha = 1f
            }
            false
        }
        parentModeButton.setOnClickListener { startOnboarding(false) }

        childModeButton.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> v.alpha = 0.75f
                MotionEvent.ACTION_UP -> v.alpha = 1f
            }
            false
        }
        childModeButton.setOnClickListener { startOnboarding(true) }
    }

    private fun initAuth() {
        val mode = AuthContextHolder.instance.mode
        F2WLogger.info("Application mode: ${mode.name}")

        when (mode) {
            AppMode.PARENT -> handleParent()
            AppMode.CHILD -> handleChild()
            else -> handleNone()
        }
    }

    private fun handleNone() {
        CustomAnimator.fade(loadingLayout, 500, 1f, 0f, object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                CustomAnimator.fade(buttonLayout, 250, 0f, 1f)
            }
        })
    }

    private fun handleChild() {
        ChildMainActivity.start(this, true)
    }

    private fun handleParent() {
        AuthCallExecutor.refreshToken(object : ICallback<Boolean> {
            override fun call(o: Boolean) {
                if (o) {
                    ParentMainActivity.start(this@PreSignInActivity, true)
                } else {
                    AuthContextHolder.instance.removeToken()
                    SignInActivity.start(this@PreSignInActivity, true)
                }
            }
        })
    }

    private fun startOnboarding(isChild: Boolean) {
        OnboardingActivity.start(this, isChild)
    }

    companion object {
        fun start(baseActivity: Activity, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseActivity, PreSignInActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }
    }
}

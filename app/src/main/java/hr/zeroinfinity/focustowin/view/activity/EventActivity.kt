package hr.zeroinfinity.focustowin.view.activity

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.*
import hr.zeroinfinity.focustowin.config.gson.GsonContext
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.retrofit.api.EventAPI
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.EventStatus
import hr.zeroinfinity.focustowin.model.internal.Duration
import hr.zeroinfinity.focustowin.service.ApplicationListenerService
import hr.zeroinfinity.focustowin.service.event.ACTIVE
import hr.zeroinfinity.focustowin.service.event.EventInfo
import hr.zeroinfinity.focustowin.service.event.FAILED
import hr.zeroinfinity.focustowin.service.event.SUCCEEDED
import hr.zeroinfinity.focustowin.view.adapter.ChoirReviewListAdapter
import hr.zeroinfinity.focustowin.view.custom.F2WDialog
import hr.zeroinfinity.focustowin.view.custom.F2WEventReviewDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.coroutines.ObsoleteCoroutinesApi
import java.util.*

@ObsoleteCoroutinesApi
class EventActivity : AuthBaseActivity(true) {

    private var event: Event? = null
    private var isDialogActive = false

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val eventInfo = intent?.getParcelableExtra<EventInfo>(EVENT_INFO_INTENT_CODE)

            when (eventInfo?.state) {
                ACTIVE -> updateTime(eventInfo.duration!!)
                FAILED -> onEventFailed()
                SUCCEEDED -> onEventSucceeded()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)

        PreferenceManager.getDefaultSharedPreferences(this).let {
            val eventJson = it.getString(EVENT_SHARED_PREFERENCES, null)
            if (eventJson == null) {
                ChildMainActivity.start(this, true)
                return
            }

            try {
                event = GsonContext.instance.fromJson(eventJson, Event::class.java)
            } catch (e: Exception) {
                it.edit().remove(EVENT_SHARED_PREFERENCES).apply()
                ChildMainActivity.start(this, true)
                return
            }
        }

        updateTime(Duration.fromSeconds(event?.duration!! * 60))

        val params = choresListView.layoutParams
        params.height = if (event?.chores?.size!! < 4) {
            event?.chores?.size!! * 55
        } else {
            130
        }

        choresListView.layoutParams = params
        choresListView.invalidate()

        choresListView.adapter = ChoirReviewListAdapter(this, event?.chores!!, true)
        progressBar.isIndeterminate = false

        when {
            event?.status == EventStatus.SUCCEEDED -> {
                onEventSucceeded()
            }

            event?.status == EventStatus.FAILED -> {
                onEventFailed()
            }

            else -> initEvent()
        }
    }

    override fun onBackPressed() {
        if (isDialogActive) return


        F2WDialog.builder(this)
            .setTitle(getString(R.string.quitting_event_title))
            .setMessage(getString(R.string.quitting_event_message))
            .setPositiveButtonText(getString(R.string.yes_label))
            .setPositiveButtonListener(View.OnClickListener {
                cancelEvent()
                isDialogActive = false
            })
            .setNegativeButtonText(getString(R.string.no_label))
            .setNegativeButtonListener(View.OnClickListener {
                isDialogActive = false
            })
            .setCanceledOnTouchOutside(false)
            .build()
            .show()
        isDialogActive = true
    }

    override fun onResume() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(EVENT_INFO_INTENT_FILTER))
        super.onResume()
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        super.onPause()
    }

    override fun onDestroy() {
        stopService(Intent(this, ApplicationListenerService::class.java))
        super.onDestroy()
    }

    private fun initEvent() {
        val i = Intent(this, ApplicationListenerService::class.java)
        i.putExtra(EVENT_INTENT_CODE, event)
        startService(i)
    }

    private fun updateTime(duration: Duration) {
        durationTextView.text = String.format("%02d:%02d:%02d", duration.hours, duration.minutes, duration.seconds)

        progressBar.progress = (100 * (duration.toSeconds().toDouble() / event?.duration!! / 60)).toFloat()
    }

    private fun onEventSucceeded() {
        if (isDialogActive) return
        isDialogActive = true

        F2WEventReviewDialog(this, {
            updateEvent()
        }, event!!, true).show()
    }

    private fun onEventFailed() {
        if (isDialogActive) return
        isDialogActive = true

        val builder = F2WDialog.builder(this)
            .setCanceledOnTouchOutside(false)
            .setTitle(getString(R.string.event_failed_title))

        if (event?.endTime?.after(Date())!!) {
            builder
                .bothPositive(true)
                .setMessage(getString(R.string.event_failed_try_again_message))
                .setPositiveButtonText(getString(R.string.try_now_label))
                .setPositiveButtonListener(View.OnClickListener {
                    updateTime(Duration.fromSeconds(event?.duration!! * 60))
                    this@EventActivity.sendBroadcast(Intent().apply {
                        this.action = EVENT_STATE_CHANGED_INTENT_FILTER
                        this.putExtra(EVENT_STATE_CHANGED_INTENT_CODE, EventInfo(ACTIVE))
                    })
                })
                .setNegativeButtonText(getString(R.string.try_later_label))
                .setNegativeButtonListener(View.OnClickListener {
                    PreferenceManager.getDefaultSharedPreferences(this)
                        .edit()
                        .remove(EVENT_SHARED_PREFERENCES)
                        .apply()

                    ChildMainActivity.start(this, true)
                })
                .build()
                .show()
        } else {
            builder
                .setMessage(getString(R.string.event_failed_better_luck_message))
                .setPositiveButtonText(getString(R.string.ok_label))
                .setPositiveButtonListener(View.OnClickListener {
                    PreferenceManager.getDefaultSharedPreferences(this)
                        .edit()
                        .remove(EVENT_SHARED_PREFERENCES)
                        .apply()

                    ChildMainActivity.start(this, true)
                })
                .build()
                .show()
        }
    }

    private fun updateEvent() {
        EventAPI.instance.update(event!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("Event ${event?.id} updated.")
                    PreferenceManager.getDefaultSharedPreferences(this)
                        .edit()
                        .remove(EVENT_SHARED_PREFERENCES)
                        .apply()

                    ChildMainActivity.start(this, true)
                },
                {
                    F2WLogger.error(it.message)

                    Snackbar.make(
                        rootView,
                        getString(R.string.no_internet_error_message),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(R.string.retry_label) {
                            updateEvent()
                        }
                        .show()
                }
            )
    }

    private fun cancelEvent() {
        LocalBroadcastManager
            .getInstance(this)
            .sendBroadcast(Intent().apply {
                this.putExtra(EVENT_STATE_CHANGED_INTENT_CODE, FAILED)
                this.action = EVENT_STATE_CHANGED_INTENT_FILTER
            })
    }

    companion object {
        fun start(baseActivity: Activity, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseActivity, EventActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseActivity.startActivity(intent)
            } else {
                baseActivity.startActivityForResult(intent, requestCode)
            }
        }

        fun start(baseFragment: Fragment, asNewTask: Boolean = true, requestCode: Int? = null) {
            val intent = Intent(baseFragment.context, EventActivity::class.java)

            if (asNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            if (requestCode == null) {
                baseFragment.startActivity(intent)
            } else {
                baseFragment.startActivityForResult(intent, requestCode)
            }
        }
    }
}

package hr.zeroinfinity.focustowin.view.adapter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import hr.zeroinfinity.focustowin.common.Refreshable
import hr.zeroinfinity.focustowin.config.CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.view.fragment.ChildFragment

class ChildInfoAdapter(fragmentManager: FragmentManager, private val children: MutableList<Child>) :
    FragmentStatePagerAdapter(fragmentManager), Refreshable<List<Child>> {

    private var fragments: MutableList<ChildFragment> = mutableListOf()

    init {
        for (child in children) {
            fragments.add(ChildFragment.newInstance(child))
        }
    }

    override fun getCount(): Int = fragments.size

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun refresh(newData: List<Child>) {
        children.clear()
        children.addAll(newData)

        for (i in 0 until newData.size) {
            val child = newData[i]

            if (i >= fragments.size) {
                fragments.add(i, ChildFragment.newInstance(child))
            } else {
                val args = Bundle()
                args.putParcelable(CHILD_INTENT_CODE, child)
                fragments[i].arguments = args
            }
        }

        if (newData.size < fragments.size) {
            fragments.subList(newData.size, fragments.size).clear()
        }

        notifyDataSetChanged()
    }
}
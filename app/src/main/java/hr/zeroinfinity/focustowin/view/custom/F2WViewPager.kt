package hr.zeroinfinity.focustowin.view.custom

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class F2WViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var scrollEnabled: Boolean = false

//    init {
//        addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
//            override fun onPageSelected(position: Int) {
//                requestLayout()
//            }
//        })
//    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (scrollEnabled) {
            super.onTouchEvent(event)
        } else false

    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (scrollEnabled) {
            super.onInterceptTouchEvent(event)
        } else false

    }

    fun setScrolling(enabled: Boolean) {
        scrollEnabled = enabled
    }

//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        var heightMeasureSpec_ = heightMeasureSpec
//        val child = getChildAt(currentItem)
//        if (child != null) {
//            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED))
//            val h = child.measuredHeight
//            heightMeasureSpec_ = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY)
//        }
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec_)
//    }
}
package hr.zeroinfinity.focustowin.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.INDEX_INTENT_CODE
import hr.zeroinfinity.focustowin.config.onSubmit
import hr.zeroinfinity.focustowin.view.adapter.TextWatcherAdapter
import hr.zeroinfinity.focustowin.view.viewmodel.SignUpViewModel
import kotlinx.android.synthetic.main.fragment_sign_up.*


class SignUpFragment : Fragment() {

    private lateinit var sharedViewModel: SignUpViewModel
    private var index: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(it).get(SignUpViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        index = arguments?.getInt(INDEX_INTENT_CODE, 0)!!

        when (index) {
            0 -> updateUI(
                R.string.sign_up_wizard_title,
                R.string.name_label,
                sharedViewModel.user.value?.name,
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            )
            1 -> updateUI(
                R.string.sign_up_wizard_title,
                R.string.surname_label,
                sharedViewModel.user.value?.surname,
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            )
            2 -> updateUI(
                R.string.sign_up_wizard_title,
                R.string.email_label,
                sharedViewModel.user.value?.email,
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            )
            3 -> updateUI(
                R.string.sign_in_wizard_password_warning,
                R.string.password_sign_up_label,
                sharedViewModel.user.value?.password,
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            )
        }
    }

    private fun updateUI(titleId: Int, fieldHintId: Int, oldValue: String?, inputType: Int) {
        titleTextView.text = context?.getString(titleId)

        input.hint = context?.getString(fieldHintId)
        input.inputType = inputType

        if (oldValue != null) {
            input.setText(oldValue)
        }

        input.addTextChangedListener(object : TextWatcherAdapter() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s == null) return

                val user = sharedViewModel.user.value
                when (index) {
                    0 -> user?.name = s.toString()
                    1 -> user?.surname = s.toString()
                    2 -> user?.email = s.toString()
                    3 -> user?.password = s.toString()
                }

                sharedViewModel.user.postValue(user)
            }
        })

        input.onSubmit {
            if (!input.text.isNullOrEmpty()) sharedViewModel.done.postValue(true)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(index: Int) =
            SignUpFragment().apply {
                arguments = Bundle().apply {
                    putInt(INDEX_INTENT_CODE, index)
                }
            }
    }
}
package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.model.Chore
import hr.zeroinfinity.focustowin.model.ChoreStatus
import kotlinx.android.synthetic.main.dialog_create_chore.*

typealias OnChoirListener = (Chore) -> Unit

class F2WCreateChoirDialog(
    context: Context,
    private val onCreateChoir: OnChoirListener,
    private var chore: Chore? = null
) : Dialog(context, R.style.AppThemeFullscreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparentHalf)))
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        setContentView(R.layout.dialog_create_chore)

        if (chore == null) {
            chore = Chore(
                null,
                difficultySeekBar.progress + Chore.MIN_POINTS,
                ChoreStatus.UNCOMPLETED,
                ""
            )
        } else {
            positiveButton.text = context.getString(R.string.edit_label)
        }

        descriptionInput.setText(chore?.name!!)
        difficultySeekBar.progress = chore?.points!! - Chore.MIN_POINTS

        positiveButton.setOnClickListener {
            if (descriptionInput.text.isEmpty()) {
                descriptionInput.error = context.getString(R.string.error_required_field)
            } else {
                chore?.name = descriptionInput.text.toString()
                chore?.points = difficultySeekBar.progress + Chore.MIN_POINTS

                onCreateChoir.invoke(chore!!)
                dismiss()
            }
        }

        negativeButton.setOnClickListener {
            dismiss()
        }
    }
}
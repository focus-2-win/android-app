package hr.zeroinfinity.focustowin.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.common.Refreshable
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.model.User

class ParentsGridAdapter(context: Context, private val parents: MutableList<User>) :
    ArrayAdapter<User>(context, R.layout.parent_grid_item, parents), Refreshable<List<User>> {

    override fun getView(position: Int, convertView: View?, p: ViewGroup): View {
        val parent = getItem(position)

        var cv = convertView
        if (convertView == null) {
            cv = LayoutInflater.from(context).inflate(R.layout.parent_grid_item, null)
        }

        if (parent?.id == AuthContextHolder.instance.getUserData()?.get("id")?.toLong()) {
            cv?.findViewById<TextView>(R.id.textView)?.text = context.getString(R.string.your_name_format, parent?.name)
        } else {
            cv?.findViewById<TextView>(R.id.textView)?.text = parent?.name
        }

        //TODO add ability to remove parent on long click if current user is family admin
        cv?.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> v.alpha = 0.75f
                MotionEvent.ACTION_UP -> v.alpha = 1f
            }
            false
        }

        return cv!!
    }

    override fun refresh(newData: List<User>) {
        parents.clear()
        parents.addAll(newData)
        notifyDataSetChanged()
    }
}
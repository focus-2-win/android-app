package hr.zeroinfinity.focustowin.view.fragment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.graphics.Point
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.EVENT_SHARED_PREFERENCES
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.gson.GsonContext
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.network.NetworkState
import hr.zeroinfinity.focustowin.config.network.RequestFailure
import hr.zeroinfinity.focustowin.config.retrofit.api.EventAPI
import hr.zeroinfinity.focustowin.config.retrofit.api.WishAPI
import hr.zeroinfinity.focustowin.model.*
import hr.zeroinfinity.focustowin.model.internal.Avatar
import hr.zeroinfinity.focustowin.model.internal.paging.datasource.EventDataSource
import hr.zeroinfinity.focustowin.model.internal.paging.datasource.factory.EventDataSourceFactory
import hr.zeroinfinity.focustowin.view.activity.EventActivity
import hr.zeroinfinity.focustowin.view.activity.QRPreviewActivity
import hr.zeroinfinity.focustowin.view.adapter.EventListAdapter
import hr.zeroinfinity.focustowin.view.adapter.WishListAdapter
import hr.zeroinfinity.focustowin.view.custom.F2WDialogWith2Inputs
import hr.zeroinfinity.focustowin.view.custom.F2WStartEventDialog
import hr.zeroinfinity.focustowin.view.viewmodel.ChildMainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_child_main.*
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.math.roundToInt

class ChildMainFragment : Fragment() {

    private lateinit var sharedViewModel: ChildMainViewModel
    private var dataSourceFactory: EventDataSourceFactory? = null
    private var eventList: LiveData<PagedList<Event>>? = null
    private lateinit var eventListAdapter: EventListAdapter

    private val executor: Executor = Executors.newFixedThreadPool(2)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(it).get(ChildMainViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_child_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val activeChild = sharedViewModel.getActiveChild()!!
        eventListAdapter = EventListAdapter(context!!, {
            if (it.status == EventStatus.PENDING && it.endTime?.after(Date())!!) {
                F2WStartEventDialog(
                    context!!,
                    it
                )
                { event ->
                    PreferenceManager.getDefaultSharedPreferences(context!!).let { sp ->
                        sp.edit().putString(EVENT_SHARED_PREFERENCES, GsonContext.instance.toJson(event)).commit()
                        EventActivity.start(this)
                    }
                }.show()
            }
        }, true)

        addNewWishButton.setOnClickListener {
            F2WDialogWith2Inputs.builder(this@ChildMainFragment.context!!)
                .setCanceledOnTouchOutside(true)
                .setTitle(getString(R.string.new_wish_title))
                .setMessage(getString(R.string.new_wish_message))
                .setInput1Placeholder(getString(R.string.description_label))
                .setInput1Optional(false)
                .setInput2Placeholder(getString(R.string.image_url_label))
                .setInput2Optional(true)
                .setPositiveButtonText(getString(R.string.create_label))
                .setNegativeButtonText(getString(R.string.cancel_label))
                .setListener { description, imageUrl, isValid ->
                    if (isValid) {
                        val wish = Wish(
                            childId = AuthContextHolder.instance.getChildId(),
                            description = description,
                            imageUrl = imageUrl,
                            status = WishStatus.PENDING
                        )

                        createWish(wish)
                    }
                }.build().show()
        }

        wishListRecyclerView.adapter = WishListAdapter(activeChild.wishList?.toMutableList()!!, null, null)
        //TODO on long click remove pending wish

        refreshChildInfo(activeChild)

        sharedViewModel.family.observe(this, Observer {
            val ac = sharedViewModel.getActiveChild()
            if (ac == null) {
                QRPreviewActivity.start(this, true, AuthContextHolder.instance.getChildId(), asNewTask = true)
                return@Observer
            }

            refreshChildInfo(ac)
            (wishListRecyclerView.adapter as WishListAdapter).refresh(ac.wishList!!)
            initDataSource(ac)
        })

        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val height = size.y

        val params = eventsRecyclerView.layoutParams
        params.height = (height * 0.82).roundToInt()
        eventsRecyclerView.layoutParams = FrameLayout.LayoutParams(params)

        eventsRecyclerView.setHasFixedSize(false)
        eventsRecyclerView.adapter = eventListAdapter

        initDataSource(activeChild)
    }

    private fun createWish(wish: Wish) {
        WishAPI.instance.create(wish)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("Wish ${it.entityId} created.")

                    val activeChild = sharedViewModel.getActiveChild()!!
                    activeChild.wishList?.add(wish)
                    (wishListRecyclerView.adapter as WishListAdapter).refresh(activeChild.wishList!!)
                },
                {
                    F2WLogger.error(it.message)

                    Snackbar.make(
                        rootView,
                        getString(R.string.no_internet_error_message),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(R.string.retry_label) {
                            createWish(wish)
                        }
                        .show()
                }
            )
    }

    private fun initDataSource(activeChild: Child) {
        dataSourceFactory?.dataSourceLiveData?.removeObservers(this)
        dataSourceFactory?.dataSourceLiveData?.value?.networkStateLiveData?.removeObservers(this)
        dataSourceFactory?.dataSourceLiveData?.value?.requestFailureLiveData?.removeObservers(this)

        dataSourceFactory = EventDataSourceFactory(EventAPI.instance, activeChild.id!!)
        dataSourceFactory?.dataSourceLiveData?.observe(this, Observer<EventDataSource> {
            it?.requestFailureLiveData?.observe(this, Observer<RequestFailure> { requestFailure ->
                if (requestFailure == null) return@Observer

                Snackbar.make(
                    this@ChildMainFragment.view!!,
                    getString(R.string.no_internet_error),
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(getString(R.string.retry_label)) {
                        requestFailure.retryable.invoke()
                    }.show()
            })

            it?.networkStateLiveData?.observe(this, Observer<NetworkState> { networkState ->
                progressBar.visibility = if (networkState == NetworkState.LOADING) View.VISIBLE else View.INVISIBLE
            })
        })

        val config = PagedList.Config.Builder()
            .setPageSize(EventAPI.PAGE_SIZE)
            .setInitialLoadSizeHint(EventAPI.PAGE_SIZE * 2)
            .setEnablePlaceholders(false)
            .build()

        eventList = LivePagedListBuilder(dataSourceFactory!!, config)
            .setFetchExecutor(executor)
            .build()

        eventList?.observe(this, Observer<PagedList<Event>> {
            eventListAdapter.submitList(it)
        })
    }

    private fun refreshChildInfo(activeChild: Child) {
        var avatar = Avatar.ALL.firstOrNull { it.id == activeChild.avatarId }
        if (avatar == null) {
            avatar = Avatar.ALL.first()
        }

        avatarImageView.setImageResource(avatar.resId)
        nameTextView.text = activeChild.username
        levelTextView.text = activeChild.level?.id.toString()
        pointsEarnedTextView.text = activeChild.pointsEarned.toString()
        pointsToGoTextView.text = (activeChild.level?.maxPoints!! - activeChild.pointsEarned!!).toString()
        (wishListRecyclerView.adapter as WishListAdapter).refresh(activeChild.wishList!!)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            ChildMainFragment()
    }
}

package hr.zeroinfinity.focustowin.view.fragment

import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import hr.zeroinfinity.focustowin.R
import hr.zeroinfinity.focustowin.config.CHILD_INTENT_CODE
import hr.zeroinfinity.focustowin.config.logger.F2WLogger
import hr.zeroinfinity.focustowin.config.network.NetworkState
import hr.zeroinfinity.focustowin.config.network.RequestFailure
import hr.zeroinfinity.focustowin.config.retrofit.api.ChildAPI
import hr.zeroinfinity.focustowin.config.retrofit.api.EventAPI
import hr.zeroinfinity.focustowin.config.retrofit.api.WishAPI
import hr.zeroinfinity.focustowin.model.Child
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.Wish
import hr.zeroinfinity.focustowin.model.WishStatus
import hr.zeroinfinity.focustowin.model.internal.paging.datasource.EventDataSource
import hr.zeroinfinity.focustowin.model.internal.paging.datasource.factory.EventDataSourceFactory
import hr.zeroinfinity.focustowin.view.activity.NewEventActivity
import hr.zeroinfinity.focustowin.view.adapter.ChildInfoAdapter
import hr.zeroinfinity.focustowin.view.adapter.EventListAdapter
import hr.zeroinfinity.focustowin.view.adapter.WishListAdapter
import hr.zeroinfinity.focustowin.view.custom.F2WApproveWishDialog
import hr.zeroinfinity.focustowin.view.custom.F2WCreateChildDialog
import hr.zeroinfinity.focustowin.view.custom.F2WDialog
import hr.zeroinfinity.focustowin.view.custom.F2WViewPager
import hr.zeroinfinity.focustowin.view.viewmodel.ParentMainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_children.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.math.roundToInt


class ChildrenFragment : Fragment() {
    private lateinit var sharedViewModel: ParentMainViewModel

    private var dataSourceFactory: EventDataSourceFactory? = null
    private var eventList: LiveData<PagedList<Event>>? = null
    private lateinit var eventListAdapter: EventListAdapter

    private val executor: Executor = Executors.newFixedThreadPool(2)

    private val selectedChild = MutableLiveData<Child>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(it).get(ParentMainViewModel::class.java)
        }

        if (arguments != null) {
            arguments?.let {
                selectedChild.value =
                    it.getParcelable(CHILD_INTENT_CODE) ?: sharedViewModel.family.value?.minors?.get(0)
            }
        } else {
            selectedChild.value = sharedViewModel.family.value?.minors?.get(0)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_children, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tabDots.setupWithViewPager(mViewPager, true)

        eventListAdapter = EventListAdapter(context!!, { onRefresh() })
        levelTextView.text = selectedChild.value?.level?.id!!.toString()

        if (selectedChild.value?.lastWeekTasksCount!! != 0) {
            tasksCompletedTextView.text = getString(
                R.string.tasks_completed_format,
                selectedChild.value?.lastWeekTasksCompletedCount!!, selectedChild.value?.lastWeekTasksCount!!
            )
            tasksCompletedTextView.textSize = 60.0f
        } else {
            tasksCompletedTextView.text = getString(R.string.none_label)
            tasksCompletedTextView.textSize = 50.0f
        }

        mViewPager.adapter =
            ChildInfoAdapter(childFragmentManager, sharedViewModel.family.value?.minors!!.toMutableList())
        mViewPager.setCurrentItem(sharedViewModel.family.value?.minors?.indexOf(selectedChild.value!!)!!, false)
        (mViewPager as F2WViewPager).setScrolling(true)

        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p: Int) = Unit

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) = Unit

            override fun onPageSelected(p0: Int) {
                selectedChild.postValue(sharedViewModel.family.value?.minors?.get(p0))
            }
        })

        editProfileButton.setOnClickListener { onEditChild() }
        addNewTaskButton.setOnClickListener { onAddNewTask() }

        wishListRecyclerView.adapter =
            WishListAdapter(mutableListOf(), {
                if (it.status?.id == WishStatus.PENDING.id) {
                    F2WApproveWishDialog(this@ChildrenFragment.context!!, { approvedWish ->
                        approvedWish.status = WishStatus.ACCEPTED
                        approvedWish.currentPoints = 0

                        updateWish(approvedWish)
                    }, it).show()
                }
            }, null) //TODO on long click remove pending wish

        selectedChild.observe(this, Observer {
            (wishListRecyclerView.adapter as WishListAdapter).refresh(it?.wishList!!)
            if (it?.wishList?.size!! > 0) {
                wishListPlaceholder.visibility = View.GONE
            } else {
                wishListPlaceholder.visibility = View.VISIBLE
            }

            levelTextView.text = it.level?.id!!.toString()
            initDataSource()
        })

        sharedViewModel.family.observe(this, Observer {
            (mViewPager.adapter as ChildInfoAdapter).refresh(it?.minors!!)

            val selected = selectedChild.value
            val child = it.minors?.find { c -> c.id == selected?.id }

            if (child == null) {
                selectedChild.postValue(it.minors?.first())
            } else {
                selectedChild.postValue(child)
            }
        })

        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val height = size.y

        val params = eventsRecyclerView.layoutParams
        params.height = (height * 0.82).roundToInt()
        eventsRecyclerView.layoutParams = FrameLayout.LayoutParams(params)

        eventsRecyclerView.setHasFixedSize(false)
        eventsRecyclerView.adapter = eventListAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NEW_EVENT_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            onRefresh()
        }
    }

    private fun updateWish(wish: Wish) {
        WishAPI.instance.update(wish)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    F2WLogger.info("Wish ${wish.id} updated.")
                    wishListRecyclerView.adapter?.notifyDataSetChanged()
                },
                {
                    F2WLogger.error(it.message)

                    Snackbar.make(
                        rootView,
                        getString(R.string.no_internet_error_message),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(R.string.retry_label) {
                            updateWish(wish)
                        }
                        .show()
                }
            )
    }

    private fun initDataSource() {
        dataSourceFactory?.dataSourceLiveData?.removeObservers(this)
        dataSourceFactory?.dataSourceLiveData?.value?.networkStateLiveData?.removeObservers(this)
        dataSourceFactory?.dataSourceLiveData?.value?.requestFailureLiveData?.removeObservers(this)

        dataSourceFactory = EventDataSourceFactory(EventAPI.instance, selectedChild.value?.id!!)
        dataSourceFactory?.dataSourceLiveData?.observe(this, Observer<EventDataSource> {
            it?.requestFailureLiveData?.observe(this, Observer<RequestFailure> { requestFailure ->
                if (requestFailure == null) return@Observer

                Snackbar.make(
                    this@ChildrenFragment.view!!,
                    getString(hr.zeroinfinity.focustowin.R.string.no_internet_error),
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(getString(hr.zeroinfinity.focustowin.R.string.retry_label)) {
                        requestFailure.retryable.invoke()
                    }.show()
            })

            it?.networkStateLiveData?.observe(this, Observer<NetworkState> { networkState ->
                progressBar.visibility = if (networkState == NetworkState.LOADING) View.VISIBLE else View.INVISIBLE
            })
        })

        val config = PagedList.Config.Builder()
            .setPageSize(EventAPI.PAGE_SIZE)
            .setInitialLoadSizeHint(EventAPI.PAGE_SIZE * 2)
            .setEnablePlaceholders(false)
            .build()

        eventList = LivePagedListBuilder(dataSourceFactory!!, config)
            .setFetchExecutor(executor)
            .build()

        eventList?.observe(this, Observer<PagedList<Event>> {
            eventListAdapter.submitList(it)
        })
    }

    private fun onAddNewTask() {
        val approvedWishes =
            selectedChild.value?.wishList?.filter { wish -> wish.status?.id!! == WishStatus.ACCEPTED.id }
        if (approvedWishes != null && approvedWishes.isNotEmpty()) {
            NewEventActivity.start(
                this,
                false,
                NEW_EVENT_ACTIVITY_REQUEST_CODE,
                selectedChild.value?.id!!,
                approvedWishes
            )
        } else {
            F2WDialog.builder(context!!)
                .setTitle(context?.getString(hr.zeroinfinity.focustowin.R.string.warning_label)!!)
                .setMessage(getString(hr.zeroinfinity.focustowin.R.string.no_approved_wish_warning))
                .setPositiveButtonText(getString(hr.zeroinfinity.focustowin.R.string.i_understand_label))
                .setPositiveButtonListener(View.OnClickListener { })
                .build()
                .show()
        }
    }

    private fun onEditChild() {
        F2WCreateChildDialog.builder(context!!)
            .setListener { name, avatarId ->
                var child = selectedChild.value!!
                child.username = name
                child.avatarId = avatarId

                editChild(child)
            }
            .setChild(selectedChild.value!!)
            .build()
            .show()
    }

    private fun editChild(child: Child) {
        ChildAPI.instance.update(child)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    selectedChild.postValue(child)

                    val family = sharedViewModel.family.value!!
                    val updatedChild = family.minors?.find { it.id == child.id }!!
                    updatedChild.username = child.username
                    updatedChild.avatarId = child.avatarId
                    sharedViewModel.family.postValue(family)
                },
                {
                    Snackbar.make(
                        this@ChildrenFragment.view!!,
                        getString(R.string.no_internet_error),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(getString(R.string.retry_label)) {
                            editChild(child)
                        }.show()
                }
            )
    }

    fun onRefresh() {
        dataSourceFactory?.dataSourceLiveData?.value?.invalidate()
    }

    companion object {
        const val NEW_EVENT_ACTIVITY_REQUEST_CODE = 1231

        @JvmStatic
        fun newInstance(child: Child? = null) =
            ChildrenFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(CHILD_INTENT_CODE, child)
                }
            }
    }
}
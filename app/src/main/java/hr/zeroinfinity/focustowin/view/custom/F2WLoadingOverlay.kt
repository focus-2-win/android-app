package hr.zeroinfinity.focustowin.view.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import hr.zeroinfinity.focustowin.R

class F2WLoadingOverlay(
    context: Context
) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.attributes?.windowAnimations = R.style.DialogAnimation


        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.loading_overlay)
        setCanceledOnTouchOutside(false)
    }

    override fun onBackPressed() = Unit
}
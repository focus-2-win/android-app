package hr.zeroinfinity.focustowin.service.event

import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import hr.zeroinfinity.focustowin.config.EVENT_INFO_INTENT_CODE
import hr.zeroinfinity.focustowin.config.EVENT_INFO_INTENT_FILTER
import hr.zeroinfinity.focustowin.config.EVENT_SHARED_PREFERENCES
import hr.zeroinfinity.focustowin.config.gson.GsonContext
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.EventStatus

class EventStateSucceeded(private val context: Context, private val offset: Int) : EventState {

    override fun elapsedSeconds(): Int = offset

    override fun handle() {
        PreferenceManager.getDefaultSharedPreferences(context).let {
            val eventJson = it.getString(EVENT_SHARED_PREFERENCES, null) ?: return

            try {
                val event = GsonContext.instance.fromJson(eventJson, Event::class.java)
                event.status = EventStatus.SUCCEEDED
                it.edit().putString(EVENT_SHARED_PREFERENCES, GsonContext.instance.toJson(event)).apply()
            } catch (e: Exception) {
                it.edit().remove(EVENT_SHARED_PREFERENCES).apply()
            }
        }

        context.sendBroadcast(Intent().apply {
            this.action = EVENT_INFO_INTENT_FILTER
            this.putExtra(EVENT_INFO_INTENT_CODE, EventInfo(SUCCEEDED))
        })
    }
}
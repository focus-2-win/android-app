package hr.zeroinfinity.focustowin.service.event

import android.os.Parcelable
import hr.zeroinfinity.focustowin.model.internal.Duration
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventInfo(
    var state: Int,
    var duration: Duration? = null
) : Parcelable
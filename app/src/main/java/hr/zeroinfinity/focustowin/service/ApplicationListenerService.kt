package hr.zeroinfinity.focustowin.service

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import hr.zeroinfinity.focustowin.config.EVENT_INTENT_CODE
import hr.zeroinfinity.focustowin.config.EVENT_STATE_CHANGED_INTENT_CODE
import hr.zeroinfinity.focustowin.config.EVENT_STATE_CHANGED_INTENT_FILTER
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.service.event.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.launch

@ObsoleteCoroutinesApi
class ApplicationListenerService : Service() {

    private lateinit var tickerChannel: ReceiveChannel<Unit>
    private lateinit var tickerJob: Job

    private val binder = LocalBinder()
    private var event: Event? = null
    private var eventState: EventState = EventStatePending.INSTANCE

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent.let {
                when (it?.getIntExtra(EVENT_STATE_CHANGED_INTENT_CODE, -1)) {
                    SUCCEEDED -> {
                        eventState = EventStateSucceeded(this@ApplicationListenerService, eventState.elapsedSeconds())
                    }
                    FAILED -> {
                        eventState = EventStateFailed(this@ApplicationListenerService, eventState.elapsedSeconds())
                    }
                    ACTIVE -> {
                        purge()
                        handleActionEventStart()
                    }
                    PAUSED -> handleActionEventPause() //TODO
                    //TODO continue
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(EVENT_STATE_CHANGED_INTENT_FILTER))
    }

    override fun onDestroy() {
        super.onDestroy()

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        purge()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        event = intent?.getParcelableExtra(EVENT_INTENT_CODE)
        handleActionEventStart()

        return super.onStartCommand(intent, flags, startId)
    }

    private fun handleActionEventStart() {
        eventState = EventStateActive(
            event!!,
            this
        )

        tickerChannel = ticker(delayMillis = 1000L, initialDelayMillis = 0L)
        tickerJob = GlobalScope.launch {
            for (e in tickerChannel) {
                eventState.handle()
            }
        }
    }

    private fun handleActionEventPause() {
        //TODO in the future
    }

    private fun purge() {
        tickerChannel.cancel()
        tickerJob.cancel()
        eventState = EventStatePending.INSTANCE
        event = null
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): ApplicationListenerService = this@ApplicationListenerService
    }
}

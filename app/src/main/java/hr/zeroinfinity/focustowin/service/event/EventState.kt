package hr.zeroinfinity.focustowin.service.event

const val PENDING = 1
const val ACTIVE = 2
const val PAUSED = 3
const val SUCCEEDED = 4
const val FAILED = 5

interface EventState {
    fun handle()

    fun elapsedSeconds(): Int
}
package hr.zeroinfinity.focustowin.service.event

class EventStatePending private constructor() : EventState {

    override fun elapsedSeconds(): Int = 0

    override fun handle() {}

    companion object {
        val INSTANCE = EventStatePending()
    }
}
package hr.zeroinfinity.focustowin.service.event

import android.content.Context
import android.content.Intent
import hr.zeroinfinity.focustowin.config.EVENT_INFO_INTENT_CODE
import hr.zeroinfinity.focustowin.config.EVENT_INFO_INTENT_FILTER
import hr.zeroinfinity.focustowin.config.EVENT_STATE_CHANGED_INTENT_CODE
import hr.zeroinfinity.focustowin.config.EVENT_STATE_CHANGED_INTENT_FILTER
import hr.zeroinfinity.focustowin.config.lifecycle.ProcessHelper
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.internal.Duration
import java.util.*

class EventStateActive(private val event: Event, private val context: Context, private val offset: Int = 0) :
    EventState {
    private val eventStartTimestamp: Date = Date()

    override fun handle() {
        val timeElapsed = (offset + (Date().time - eventStartTimestamp.time) / 1000).toInt()

        when {
            timeElapsed == event.duration!! -> eventFinished()
            ProcessHelper.areOtherAppsRunning(
                context,
                context.packageName,
                eventStartTimestamp
            ) -> eventFailed()
            else -> eventActive(timeElapsed)
        }
    }

    override fun elapsedSeconds(): Int = (offset + (Date().time - eventStartTimestamp.time) / 1000).toInt()

    private fun eventActive(timeElapsed: Int) {
        context.sendBroadcast(Intent().apply {
            this.action = EVENT_INFO_INTENT_FILTER
            this.putExtra(
                EVENT_INFO_INTENT_CODE,
                EventInfo(ACTIVE, Duration.fromSeconds(event.duration!! * 60 - timeElapsed))
            )
        })
    }

    private fun eventFinished() {
        context.sendBroadcast(Intent().apply {
            this.action = EVENT_STATE_CHANGED_INTENT_FILTER
            this.putExtra(EVENT_STATE_CHANGED_INTENT_CODE, SUCCEEDED)
        })
    }

    private fun eventFailed() {
        context.sendBroadcast(Intent().apply {
            this.action = EVENT_STATE_CHANGED_INTENT_FILTER
            this.putExtra(EVENT_STATE_CHANGED_INTENT_CODE, FAILED)
        })
    }
}
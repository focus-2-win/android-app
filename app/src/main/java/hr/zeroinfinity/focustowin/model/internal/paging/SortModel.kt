package hr.zeroinfinity.focustowin.model.internal.paging

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SortModel(

    @SerializedName("sorted")
    @Expose
    val sorted: Boolean? = null,

    @SerializedName("unsorted")
    @Expose
    val unsorted: Boolean? = null
)
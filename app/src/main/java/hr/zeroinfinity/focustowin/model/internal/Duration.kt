package hr.zeroinfinity.focustowin.model.internal

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Duration(
    val seconds: Int,
    val minutes: Int,
    val hours: Int
) : Parcelable {

    fun toSeconds(): Int = seconds + minutes * 60 + hours * 60 * 60

    companion object {
        fun fromSeconds(_seconds: Int): Duration {
            val hours = _seconds / 3600
            var seconds = _seconds - hours * 3600
            val minutes = seconds / 60
            seconds -= minutes * 60

            return Duration(seconds, minutes, hours)
        }
    }
}
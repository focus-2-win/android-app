package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import android.support.v7.util.DiffUtil
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

import java.util.*

@Parcelize
data class Event(

    @SerializedName("id")
    @Expose
    var id: Long?,

    @SerializedName("wishId")
    @Expose
    var wishId: Long?,

    @SerializedName("startTime")
    @Expose
    var startTime: Date?,

    @SerializedName("endTime")
    @Expose
    var endTime: Date?,

    @SerializedName("duration")
    @Expose
    var duration: Int?,

    @SerializedName("status")
    @Expose
    var status: EventStatus?,

    @SerializedName("multiplier")
    @Expose
    var multiplier: Int?,

    @SerializedName("points")
    @Expose
    var points: Int?,

    @SerializedName("chores")
    @Expose
    var chores: MutableList<Chore>?
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Event

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<Event> = object : DiffUtil.ItemCallback<Event>() {

            override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
                return when {
                    oldItem.id != newItem.id -> false
                    //oldItem.wishId != newItem.wishId -> false
                    //oldItem.startTime != newItem.startTime -> false
                    //oldItem.endTime != newItem.endTime -> false
                    //oldItem.duration != newItem.duration -> false
                    oldItem.status != newItem.status -> false
                    //oldItem.multiplier != newItem.multiplier -> false
                    else -> true
                }
            }
        }
    }
}
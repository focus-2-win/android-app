package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Wish(

    @SerializedName("id")
    @Expose
    var id: Long? = null,

    @SerializedName("childId")
    @Expose
    var childId: Long? = null,

    @SerializedName("currentPoints")
    @Expose
    var currentPoints: Int? = null,

    @SerializedName("targetPoints")
    @Expose
    var targetPoints: Int? = null,

    @SerializedName("description")
    @Expose
    var description: String? = null,

    @SerializedName("events")
    @Expose
    var events: MutableList<Event>? = null,

    @SerializedName("imageUrl")
    @Expose
    var imageUrl: String? = null,

    @SerializedName("wishStatus")
    @Expose
    var status: WishStatus? = null
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Wish

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }
}
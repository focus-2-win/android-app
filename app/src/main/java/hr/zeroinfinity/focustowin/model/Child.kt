package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Child(

    @SerializedName("id")
    @Expose
    var id: Long?,

    @SerializedName("username")
    @Expose
    var username: String?,

    @SerializedName("avatarId")
    @Expose
    var avatarId: Int?,

    @SerializedName("pointsEarned")
    @Expose
    var pointsEarned: Int?,

    @SerializedName("level")
    @Expose
    var level: Level?,

    @SerializedName("wishList")
    @Expose
    var wishList: MutableList<Wish>?,

    @SerializedName("lastWeekTasksCount")
    @Expose
    var lastWeekTasksCount: Int? = null,

    @SerializedName("lastWeekTasksCompletedCount")
    @Expose
    var lastWeekTasksCompletedCount: Int? = null
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Child

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }
}
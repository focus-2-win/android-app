package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class ChoreStatus(

    @SerializedName("id")
    @Expose
    var id: Int?,

    @SerializedName("name")
    @Expose
    var name: String?
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ChoreStatus

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }

    companion object {
        val UNCOMPLETED = ChoreStatus(1, "Uncompleted")
        val COMPLETED = ChoreStatus(2, "Completed")
    }
}
package hr.zeroinfinity.focustowin.model.internal

import android.content.Context
import android.graphics.drawable.Drawable
import hr.zeroinfinity.focustowin.R

data class Avatar(
    val id: Int,
    val resId: Int
) {
    fun getDrawable(context: Context): Drawable? = context.getDrawable(resId)

    companion object {
        val ALL = listOf(
            Avatar(1, R.drawable.avatar1),
            Avatar(2, R.drawable.avatar2),
            Avatar(3, R.drawable.avatar3),
            Avatar(4, R.drawable.avatar4),
            Avatar(5, R.drawable.avatar5),
            Avatar(6, R.drawable.avatar6),
            Avatar(7, R.drawable.avatar7),
            Avatar(8, R.drawable.avatar8),
            Avatar(9, R.drawable.avatar9),
            Avatar(10, R.drawable.avatar10),
            Avatar(11, R.drawable.avatar11),
            Avatar(12, R.drawable.avatar12),
            Avatar(13, R.drawable.avatar13),
            Avatar(14, R.drawable.avatar14),
            Avatar(15, R.drawable.avatar15)
        )
    }
}
package hr.zeroinfinity.focustowin.model.internal.paging.datasource.factory

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import hr.zeroinfinity.focustowin.config.retrofit.api.EventAPI
import hr.zeroinfinity.focustowin.model.Event
import hr.zeroinfinity.focustowin.model.internal.paging.datasource.EventDataSource

class EventDataSourceFactory(private val API: EventAPI, private val childId: Long) : DataSource.Factory<Int, Event>() {

    val dataSourceLiveData: MutableLiveData<EventDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Event> {
        val dataSource = EventDataSource(API, childId)

        dataSourceLiveData.postValue(dataSource)

        return dataSource
    }
}
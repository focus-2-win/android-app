package hr.zeroinfinity.focustowin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Request<T>(
    @SerializedName("entityId")
    @Expose
    val entityId: T?
)
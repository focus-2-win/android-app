package hr.zeroinfinity.focustowin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Response<T>(

    @SerializedName("valid")
    @Expose
    val valid: Boolean,

    @SerializedName("errors")
    @Expose
    val errors: Map<String, String>?,

    @SerializedName("entityId")
    @Expose
    val entityId: T?
)

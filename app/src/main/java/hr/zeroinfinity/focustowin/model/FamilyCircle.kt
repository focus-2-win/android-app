package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class FamilyCircle(

    @SerializedName("id")
    @Expose
    var id: Long?,

    @SerializedName("familyList")
    @Expose
    var familyList: MutableList<Family>?
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FamilyCircle

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }
}
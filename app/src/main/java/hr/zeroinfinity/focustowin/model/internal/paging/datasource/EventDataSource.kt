package hr.zeroinfinity.focustowin.model.internal.paging.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import hr.zeroinfinity.focustowin.config.network.NetworkState
import hr.zeroinfinity.focustowin.config.network.RequestFailure
import hr.zeroinfinity.focustowin.config.network.Retryable
import hr.zeroinfinity.focustowin.config.retrofit.api.EventAPI
import hr.zeroinfinity.focustowin.model.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EventDataSource(private val API: EventAPI, private val childId: Long) : PageKeyedDataSource<Int, Event>() {

    val requestFailureLiveData: MutableLiveData<RequestFailure> = MutableLiveData()
    val networkStateLiveData: MutableLiveData<NetworkState> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Event>) {
        val page = 0

        networkStateLiveData.postValue(NetworkState.LOADING)
        API
            .getByChildIdPageable(childId, page, params.requestedLoadSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    networkStateLiveData.postValue(NetworkState.LOADED)
                    callback.onResult(response.content!!, 0, response.totalElements!!, null, page + 1)
                },
                { error ->
                    networkStateLiveData.postValue(NetworkState.FAILED)
                    handleError({ loadInitial(params, callback) }, error)
                }
            )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Event>) {
        val page = params.key

        networkStateLiveData.postValue(NetworkState.LOADING)
        API
            .getByChildIdPageable(childId, page, params.requestedLoadSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    networkStateLiveData.postValue(NetworkState.LOADED)
                    callback.onResult(response.content!!, page + 1)
                },
                { error ->
                    networkStateLiveData.postValue(NetworkState.FAILED)
                    handleError({ loadAfter(params, callback) }, error)
                }
            )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Event>) {
        //ignorable
    }

    private fun handleError(retryable: Retryable, t: Throwable) {
        requestFailureLiveData.postValue(RequestFailure(retryable, t.message!!))
    }
}
package hr.zeroinfinity.focustowin.model.internal.paging

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PageableModel(

    @SerializedName("sortModel")
    @Expose
    var sortModel: SortModel? = null,

    @SerializedName("offset")
    @Expose
    var offset: Int? = null,

    @SerializedName("pageSize")
    @Expose
    var pageSize: Int? = null,

    @SerializedName("pageNumber")
    @Expose
    var pageNumber: Int? = null,

    @SerializedName("paged")
    @Expose
    var paged: Boolean? = null,

    @SerializedName("unpaged")
    @Expose
    var unpaged: Boolean? = null
)
package hr.zeroinfinity.focustowin.model.internal.paging

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PageModel<T>(

    @SerializedName("content")
    @Expose
    val content: List<T>? = null,

    @SerializedName("pageableModel")
    @Expose
    val pageableModel: PageableModel? = null,

    @SerializedName("totalElements")
    @Expose
    val totalElements: Int? = null,

    @SerializedName("last")
    @Expose
    val last: Boolean? = null,

    @SerializedName("totalPages")
    @Expose
    val totalPages: Int? = null,

    @SerializedName("size")
    @Expose
    val size: Int? = null,

    @SerializedName("number")
    @Expose
    val number: Int? = null,

    @SerializedName("sortModel")
    @Expose
    val sortModel: SortModel? = null,

    @SerializedName("first")
    @Expose
    val first: Boolean? = null,

    @SerializedName("numberOfElements")
    @Expose
    val numberOfElements: Int? = null
)
package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Family(

    @SerializedName("id")
    @Expose
    var id: Long?,

    @SerializedName("parents")
    @Expose
    var parents: MutableList<User>?,

    @SerializedName("minors")
    @Expose
    var minors: MutableList<Child>?
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Family

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }
}
package hr.zeroinfinity.focustowin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class User(

    @SerializedName("id")
    @Expose
    var id: Long? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("surname")
    @Expose
    var surname: String? = null,

    @SerializedName("email")
    @Expose
    var email: String? = null,

    @SerializedName("password")
    @Expose
    var password: String? = null,

    @SerializedName("isFamilyAdmin")
    @Expose
    var isFamilyAdmin: Boolean? = null
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }

    override fun toString(): String {
        return "User(id=$id, name=$name, surname=$surname, email=$email, password=$password, isFamilyAdmin=$isFamilyAdmin)"
    }


}
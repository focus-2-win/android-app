package hr.zeroinfinity.focustowin

import android.app.Application
import android.arch.lifecycle.ProcessLifecycleOwner
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.preference.PreferenceManager
import hr.zeroinfinity.focustowin.config.API_KEY
import hr.zeroinfinity.focustowin.config.auth.AuthContextHolder
import hr.zeroinfinity.focustowin.config.lifecycle.MyLifecycleListener
import hr.zeroinfinity.focustowin.config.network.NetworkCallbackImpl

class FocusToWinApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        setupLifecycleListener()
        setupNetworkCallback()

        AuthContextHolder.init(PreferenceManager.getDefaultSharedPreferences(this))
        API_KEY = getString(R.string.API_KEY)
    }

    private fun setupNetworkCallback() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return
        }

        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.registerDefaultNetworkCallback(NetworkCallbackImpl)
    }

    private fun setupLifecycleListener() {
        ProcessLifecycleOwner.get().lifecycle
            .addObserver(MyLifecycleListener)
    }
}
package hr.zeroinfinity.focustowin.common

@FunctionalInterface
interface ICallback<T> {
    fun call(o: T)
}
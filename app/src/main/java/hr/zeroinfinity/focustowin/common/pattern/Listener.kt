package hr.zeroinfinity.focustowin.common.pattern

@FunctionalInterface
interface Listener<T> {
    fun update(data: T)
}

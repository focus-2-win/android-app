package hr.zeroinfinity.focustowin.common

interface Refreshable<T> {
    fun refresh(newData: T)
}
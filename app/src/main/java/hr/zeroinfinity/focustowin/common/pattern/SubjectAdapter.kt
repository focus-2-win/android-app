package hr.zeroinfinity.focustowin.common.pattern

import java.util.*

class SubjectAdapter<T> : Subject<T> {

    private val listeners = ArrayList<Listener<T>>()

    override fun addListener(listener: Listener<T>) {
        listeners.add(listener)
    }

    override fun removeListener(listener: Listener<T>) {
        listeners.remove(listener)
    }

    override fun notifyListeners(data: T) {
        for (l in listeners) {
            l.update(data)
        }
    }
}

package hr.zeroinfinity.focustowin.common

interface IStateful<T> {
    fun changeState(newState: T)
    fun resetState()
}